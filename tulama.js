/* 
  TU lAma - let Alma be more adroit

  content script
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

var toastdiv;
var deltoast;
var isTuw = false;
var settings = {};
var wcols = ['#ffffff','#008000','#000080','#808000','#c43e00','#E00009'];

function debug(msg,level = 1){
 try {
  return new Promise(function(resolv, reject){
    if (!!settings.debugLevel && settings.debugLevel >= level || level == 0){
      let iframe = newMde?'nMDE':(newMDEWrapper?'mdeW':(mde?'oMDE':(newLayout?'nLay':'oLay')));
      if (typeof msg === 'string')
        resolv(iframe+': '+msg);
      else if (typeof msg === 'object' && !(msg instanceof KeyboardEvent)){
        msg = {iFrame: iframe, ...msg};
        resolv(msg);
      } else {
        console.log(iframe);
        resolv(msg);
      }
    }
  });
 } catch(e) { console.log(e); resolv(msg); }
}

function hashCode(s) {
  var h = 0, l = s.length, i = 0;
  if ( l > 0 )
    while (i < l)
      h = (h << 5) - h + s.charCodeAt(i++) | 0;
  if (h < 0) h = -h;
  return h.toString(16);
}

function toast(msg='', col='#eee', sek=10){
  // message am unteren rand ~10 sec lang, col als hintergrund
  // einmal drauf clicken: nachricht beliebt stehen
  // ein zweitesmal drauf clicken: nachricht wird entfernt.
  if (toastdiv == undefined){
    toastdiv = $("<div id='tu_lAma_toast' style='position: fixed; width: 100%; bottom: 0; display: flex; z-index: 1000; '></div>");
    $('body').append(toastdiv);
  }
  let msgId = 'msg_'+hashCode(msg);
  if (!!msg && (toastdiv.find('#'+msgId).length == 0 || toastdiv.find('div').filter(function(i,div){return div.innerHTML == msg;}).length == 0)){
    var toastobj = $(`<div id='${msgId}' style='display: none; background: ${col}; padding: 5px 5px 5px 20px; flex-grow: 1; '>${msg}</div>`);
    toastdiv.append(toastobj);
    toastobj.fadeIn(1000).delay(sek*1000).fadeOut(1000);
    var deltoast = setTimeout(function(){
      toastobj.remove();
    }, sek*1000);
    toastobj.click(function(){
      clearTimeout(deltoast);
      toastobj.stop(true).show().click(function(){ toastobj.remove();});
    });
  }
}

function search_items(){
  var title = $('#MarcEditorView\\.title:contains(Bestand), #MarcEditorView\\.title:contains(Holding)');
  if (title.length == 0){
    title = $('#MarcEditorView\\.title:contains(Bibliografisch), #MarcEditorView\\.title:contains(Bibliographic)');
  }
  if (title.length == 1){
    var idre = /\((\d*)\)/;
    var match = idre.exec(title.text());
    if (match && match[1] != ''){
      debug(match[1],2).then(function(m){console.log(m)});
      browser.runtime.sendMessage({action: "search", what: "ITEM", id: match[1]});
    }
  } else {
    return false;
  }
}

function setSimpleSearchKey(keyStr){
  var label = findSimpleSearchKeyLabel(keyStr);
  if(keyStr && label){
    setSimpleSearchKeyAndLabel(keyStr, label);
  }
}

function setSimpleSearchKeyAndLabel(searchKey, searchLabel){
  $('#simpleSearchKey').val(searchKey);
  $('#simpleSearchKeyDisplay').html(searchLabel);
  $('#simpleSearchKey').change();
  $('#simpleSearchKeyDisplay').parent('button').attr("title",searchLabel);
}

function findSimpleSearchKeyLabel(keyStr){
  var label;
  $('.simpleSearchKeys').each(function(){
     var searchKey = $(this).data("search-key");
     var searchLabel = $(this).data("search-label");
     if(searchKey === keyStr) label= searchLabel;
     return;
  });
  return label;
}

var retries = 0;
function addButtons(){
  if (!settings.addLinks){
    if ($('table.MenuIconBarUxp').length == 0 && retries < 10){
      retries++;
      setTimeout(addButtons,1000);
    } else {
      var g2i= $('#MenuIconBar\\.Go2Items');
      if (g2i.length == 1){
        var button = g2i.closest('td');
      } else {
        var button = $('<td style="vertical-align: top;" align="left"><div tabindex="0" class="gwt-PushButton new-Design gwt-PushButton-up" role="button" style="width: auto; height: auto; padding-left: 16px; padding-right: 16px;" title="zu den Exemplaren des Titels" aria-pressed="false"><input tabindex="-1" role="presentation" style="opacity: 0; height: 1px; width: 1px; z-index: -1; overflow: hidden; position: absolute;" type="text"><div class="html-face"><span id="MenuIconBar.Go2Items" class="icon-alma-ex-alma-book-phy font20 uxfBlueDarkHover uxfBlue"></span></div></div></td>');
        $('table.MenuIconBarUxp table.gwt-MenuBar-horizontal table tbody tr').append(button);
        button.click(search_items);
      }
    }
  }
}

function addRecordViewLinks(){
  try{
  let isHolding = ($('#pageBeanRegistryType').text() == 'marc21_holding');
  if (!settings.addLinks){
    debug('add links',2).then(function(m){console.log(m)});
    var backButton = $('#PAGE_BUTTONS_cbuttonback').parent();
    if (backButton.length > 0){
      var mms_id = $('#pageBeanmmsId').text();
      if ($('#PAGE_BUTTONS_cbuttonitems').length == 0){
        var itemsButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonitems" title="' + browser.i18n.getMessage('physicalItems') + '" value="' + browser.i18n.getMessage('physicalItems') + '" name="page.buttons.operation">' + browser.i18n.getMessage('physicalItems') + '</button></div>');
        backButton.before(itemsButton);
        itemsButton.on("click",function(){
          browser.runtime.sendMessage({
            action: 'search',
            what: 'ITEM',
            id: mms_id
          });
          return false;
        });
      }
      if (isHolding){
        if ($('#PAGE_BUTTONS_cbuttonbibmms').length == 0){
          var bibButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonbibmms" title="' + browser.i18n.getMessage('bib_mms') + '" value="' + browser.i18n.getMessage('bib_mms') + '" name="page.buttons.operation">' + browser.i18n.getMessage('bib_mms') + '</button></div>');
          backButton.before(bibButton);
          bibButton.on("click",function(){
            browser.runtime.sendMessage({
              action: 'search',
              what: 'BIB_MMS',
              id: mms_id
            });
            return false;
          });
        }
      } else {
        if ($('#PAGE_BUTTONS_cbuttonholdings').length == 0){
          var holButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonholdings" title="' + browser.i18n.getMessage('holding') + '" value="' + browser.i18n.getMessage('holding') + '" name="page.buttons.operation">' + browser.i18n.getMessage('holding') + '</button></div>');
          backButton.before(holButton);
          holButton.on("click",function(){
            browser.runtime.sendMessage({
              action: 'search',
              what: 'IEP',
              id: mms_id
            });
            return false;
          });
        }
      }
    }
  }
  if (!isHolding && !!settings.addPortfolio && $('#PAGE_BUTTONS_cbuttonAddPf').length == 0){
    var addPfButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonAddPf" title="' + browser.i18n.getMessage('addPf') + '" value="' + browser.i18n.getMessage('addPf') + '" name="page.buttons.operation">' + browser.i18n.getMessage('addPf') + '</button></div>');
    backButton.before(addPfButton);
    addPfButton.on("click",function(){
      var f856 = $("span[id$='_COL_tag']:contains(856)").closest('tr').find("[id$='_COL_value']:contains(Resolving-System)");
      if (f856.length == 0)
        f856 = $("span[id$='_COL_tag']:contains(856)").closest('tr').find("[id$='_COL_value']:contains(Volltext):contains(text/html)");
      if (f856.length == 0)
        f856 = $("span[id$='_COL_tag']:contains(856)").closest('tr').find("[id$='_COL_value']:contains(Volltext)");
      var link = f856.find('a').get(0).href;
      if (f856.text()[0] != '4' || !link)
        link = '';
      window.location.href = '/ng/page;u=%2Frep%2Faction%2FpageAction.ecreation.eresource_creation.xml.do%3FxmlFileName%3Decreation.eresource_creation.xml&pageViewMode%3DEdit&operation%3DLOAD&pageBean.mmsId%3D'
        + $('#pageBeanmmsId').text()+'&pageBean.portfolioCreationType%3DUSE_EXISTING&RenewBean%3Dtrue&pageBean.currentUrl%3D&pageViewMode%3DEdit&operation%3DLOAD&pageBean.mmsId%3D'
        + $('#pageBeanmmsId').text()+'%26pageBean.portfolioCreationType%3DUSE_EXISTING%26RenewBean%3Dtrue%26pageBean.currentUrl%3D%26pageBean.electronicMaterialType%3DBOOK%26pageBean.url%3D'
        + encodeURIComponent(link);
      return false;
    });
  }
}catch(err){console.log(err)}
}

var curCatLevel;

function recordLoaded(){
  if (!settings.keepCatLevel){
    debug('keepCatLevel',2).then(function(m){console.log(m)});
    var curLevel = $('div.gwt-HTML:contains(Aktuelle Katalogisierungs-Ebene), div.gwt-HTML:contains(Current cataloging level)');
    if (curLevel.length > 0){
      debug(curLevel,2).then(function(m){console.log(m)});
      debug(curLevel.text(),2).then(function(m){console.log(m)});
      var re = /(Aktuelle Katalogisierungs-Ebene|Current cataloging level) +\[(\d*)\]/;
      var match = re.exec(curLevel.text());
      debug(match,2).then(function(m){console.log(m)});
      if (match[2] != undefined){
        curCatLevel = match[2];
        debug('current level: '+curCatLevel,2).then(function(m){console.log(m)});
        if (curLevel.closest('tr').find("select.gwt-ListBox option[value='"+curCatLevel+"']").length < 1) curCatLevel = '20';
        curLevel.closest('tr').find("select.gwt-ListBox option[value='"+curCatLevel+"']").prop('selected',true).trigger("change");
      } else {
        debug(match,2).then(function(m){console.log(m)});
      }
    }
  }
}

function gotoFirstEmpty(){
    if (!settings.goto1stEmpty && $("[id^='FieldTagBox.tag.LDR']").length > 0){
      var firstEmpty = $("input[id^='FieldTagBox.tag..']");
      if (firstEmpty.length == 0){
        firstEmpty = $("div[id^='MarcEditorPresenter.textArea'] textarea").filter(function(){
          if (this.value.trim() == '$$a' || this.value.trim() == ''){
            if (!this.closest("[id^='MarcEditorPresenter.textArea']").id.startsWith('MarcEditorPresenter.textArea.00'))
              this.value = '$$a ';
            return true;
          } else {
            return false;
          }    
        });
      }
      debug(firstEmpty,2).then(function(m){console.log(m)});
      if (firstEmpty.length > 0){
        firstEmpty.eq(0).click().focus();
        firstEmpty.get(0).scrollIntoView(true);
      }
    }
}

var ce = new KeyboardEvent("keydown", {bubbles : true, cancelable : true, key : "e", charCode :0, keyCode: 69, ctrlKey : true});

var click = document.createEvent('HTMLEvents');
click.initEvent('click', true, true);

var change = document.createEvent('HTMLEvents');
change.initEvent('keyup', false, true);

var change1 = document.createEvent('HTMLEvents');
change1.initEvent('change', false, true);

var input = document.createEvent('HTMLEvents');
input.initEvent('input', false, true);

function load_template(templateName){
  // MDE erweitern aus vorlage
  if (!!templateName && $("[id^='FieldTagBox.tag.LDR']").length > 0){
    if (newMde || newMDEWrapper){
      browser.runtime.sendMessage({action: "loadMdeTemplate"});
    } else {
      var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
          if (mutation.removedNodes.length >= 1 && $(mutation.removedNodes).find('#MdEditor\\.PopupMsg\\.LOADING').length >= 1){
            var option = $("div.gwt-DialogBox select.gwt-ListBox option:contains(" + templateName + ")");
            if (option.length > 0){
              option.prop('selected',true).trigger('change');
              $("div.gwt-DialogBox button:contains('Ok')").get(0).dispatchEvent(click);
            }
          }
        });
      });
      observer.observe($('body').get(0),{childList:true,subtree:false});
      obstimer = setTimeout(function(){
        debug('load template observer timed out!').then(function(m){console.log(m)});
        observer.disconnect();
      },30000);
    }
    document.dispatchEvent(ce);
  }
}

var konksuche = '';

function keyscan( event ) {

  function getKeywords(){
    var selection = new Array;
    $("div[id^='MarcEditorPresenter.textArea']")
        .filter('[id*="a.600"],[id*="a.610"],[id*="a.611"],[id*="a.630"],[id*="a.650"],[id*="a.653"],[id*="a.655"],[id*="a.689"]')
        .find('textarea').each(function(i,v){
      if (match = $(v).val().match(/\$\$a (.*?)( \$\$|$)/))
        selection.push(match[1]);
    });
    return [...new Set(selection)].join('* ')+'*';
  }

  function isbn_suche(){
    var isbns = new Array;
    var isbn020 = $("div[id^='MarcEditorPresenter.textArea.020'] textarea");
    var isbn7xx = $("div[id^='MarcEditorPresenter.textArea']")
        .filter('[id*="a.765"],[id*="a.767"],[id*="a.770"],[id*="a.772"],[id*="a.773"],[id*="a.774"],[id*="a.775"],[id*="a.776"],[id*="a.777"],[id*="a.780"],[id*="a.785"],[id*="a.786"],[id*="a.787"]')
        .find('textarea');
    debug(isbn020,2).then(function(m){console.log(m)});
    debug(isbn7xx,2).then(function(m){console.log(m)});
    isbn020.each(function(i,ta){
      var isbn = ta.value.match(/\$\$[az] *([0-9X\-]{10,}) *(\$\$.*)?$/);
      if (isbn){
        isbns.push(isbn[1].replace('-',''));
      }
      debug(isbn,2).then(function(m){console.log(m)});
      debug(ta.value,2).then(function(m){console.log(m)});
    });
    isbn7xx.each(function(i,ta){
      var isbn = ta.value.match(/\$\$z *([0-9X\-]{10,}) *(\$\$.*)?$/);
      if (isbn){
        isbns.push(isbn[1].replace('-',''));
      }
      debug(isbn,2).then(function(m){console.log(m)});
      debug(ta.value,2).then(function(m){console.log(m)});
    });
    if (isbns.length > 0)
      act.autocomplete('search',isbns.join(','));
    else {
      act.autocomplete('search','dasistkeineisbnnummer');
      browser.runtime.sendMessage({
        action: "toast",
        message: "keine ISBNs zur Konkordanzsuche vorhanden, suche nur nach Titel ",
        color: '#ffff8f',
        duration: 20
      });
    }
  }

  debug( event, 3).then(function(m){console.log(m)});
  let strgAlt = (event.altKey || event.metaKey) && event.ctrlKey;
  if (event.type == "keydown" && event.keyCode == 84 && strgAlt){
    load_template(settings.templateName);
  } else if (event.type == "keydown" &&
    ( event.keyCode == 83 && event.ctrlKey       // save (draft)
      || event.keyCode == 73 && !event.ctrlKey && (event.altKey || event.metaKey)   // new item
      || event.keyCode == 82 && event.ctrlKey && (event.altKey || event.metaKey))){ // save and release
    check852c();
  } else if (mde && event.keyCode == 89 && strgAlt){
    event.stopPropagation();
    event.preventDefault();
    if (event.type == "keydown"){
      createDiff();
    }
  } else if (diffdiv && event.keyCode == 27 && event.type == "keydown"){
    diffdiv.remove();
    diffdiff = false;
  } else if (mde && ((!!settings.gndUrl || !!settings.lobidUrl) && event.target.parentNode != undefined
    && ($(event.target).closest("tr").filter("tr:has([id^='MarcEditorPresenter.textArea.971'])").filter(function(){
      let ind1 = $(this).find("[id^='FieldIndicatorBox.tag.971'][id$='.1']").val();
      return ind1 == "0" || ind1 == "1" || ind1 == "2";
    }).length > 0 
    || !!settings.creatorAC && $(event.target).closest("tr").filter("tr:has([id^='MarcEditorPresenter.textArea.100'],[id^='MarcEditorPresenter.textArea.700'])").filter(function(){
      let ind1 = $(this).find("[id^='FieldIndicatorBox.tag'][id$='.1']").val();
      return ind1 == "0" || ind1 == "1" || ind1 == "2";
    }).length > 0)
    || isTuw && event.target.parentNode != undefined && $(event.target).closest("tr").filter("tr:has([id^='MarcEditorPresenter.textArea.971'])").filter(function(){
      let ind1 = $(this).find("[id^='FieldIndicatorBox.tag.971'][id$='.1']").val();
      return ind1 == "5";
    }).length > 0)){
    var act = $(event.target );
    var aci = act.autocomplete( "instance" );
    if (aci == undefined)
      var acwv = undefined;
    else
      var acwv = act.autocomplete('widget').is(':visible');
    switch( event.keyCode ){
      case $.ui.keyCode.LEFT:
        // debug( "left" ).then(function(m){console.log(m)});
        break;
      case $.ui.keyCode.RIGHT:
        // debug( "right" ).then(function(m){console.log(m)});
        break;
      case $.ui.keyCode.UP:
        if (acwv){
          // up und down zum autocomplete umlenken
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown") aci._keyEvent('previous', event);
          return false;
        }
        break;
      case $.ui.keyCode.DOWN:
        if (acwv){
          // debug( "down" ).then(function(m){console.log(m)});
          event.stopImmediatePropagation();
          event.preventDefault();
          if (event.type == "keydown") aci._keyEvent('next', event);
          return false;
        }
        break;
      case $.ui.keyCode.ENTER:
      case $.ui.keyCode.TAB:
        aci.menu.active && (event.type == "keydown", aci.menu.select(event), event.stopPropagation(), event.preventDefault());
        break;
      case $.ui.keyCode.ESCAPE:
        aci.menu.element.is(':visible') && (aci.isMultiLine || aci._value(aci.term), aci.close(event), event.stopPropagation(), event.preventDefault());
        break;
    }
  } else if (mde && isTuw && !!settings.tuwsysUrl && (event.target.parentNode != undefined
    && $(event.target).closest('[id^="MarcEditorPresenter.textArea"]').attr('id').startsWith('MarcEditorPresenter.textArea.983') || konksuche != '')){
    var act = $(event.target );
    var aci = act.autocomplete( "instance" );
    if (aci == undefined)
      var acwv = undefined;
    else
      var acwv = act.autocomplete('widget').is(':visible');
    switch( event.keyCode ) {
      case $.ui.keyCode.LEFT:
        // debug( "left" ).then(function(m){console.log(m)});
        break;
      case $.ui.keyCode.RIGHT:
        // debug( "right" ).then(function(m){console.log(m)});
        break;
      case $.ui.keyCode.UP:
        if (acwv){
          // up und down zum autocomplete umlenken
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown") aci._keyEvent('previous', event);
          return false;
        }
        break;
      case $.ui.keyCode.DOWN:
        if (acwv){
          // debug( "down" ).then(function(m){console.log(m)});
          event.stopImmediatePropagation();
          event.preventDefault();
          if (event.type == "keydown") aci._keyEvent('next', event);
          return false;
        }
        break;
      case $.ui.keyCode.ENTER:
      case $.ui.keyCode.TAB:
        aci.menu.active && (event.type == "keydown", aci.menu.select(event), event.stopPropagation(), event.preventDefault());
        break;
      case $.ui.keyCode.ESCAPE:
        aci.menu.element.is(':visible') && (aci.isMultiLine || aci._value(aci.term), aci.close(event), event.stopPropagation(), event.preventDefault());
        break;
      case 68:
      case $.ui.keyCode.BACKSPACE:
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            act.val('$$a ');
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 71: // Ctrl-Alt-G Geographikum
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            var loc = act.val().match(/(.*?)\$\$g *(.*?) *(\$\$.*)?$/);
            if (!loc){
              act.val(act.val().trim() + ' $$g ');
            } else {
              if (loc[3] == undefined)
                act.val(loc[1] + '$$g ');
              else
                act.val(loc[1] + loc[3] + ' $$g ');
            }
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 65: // Ctrl-Alt-A Sprachcode
      case 76: // Ctrl-Alt-L Sprachcode von linked data belegt...
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown" && event.keyCode==76 || event.type == "keyup" && event.keyCode==65){
            var lang = act.val().match(/(.*?)\$\$s *(.*?) *(\$\$.*)?$/);
            // debug(lang).then(function(m){console.log(m)});
            // debug(act.val().substr(4,7)).then(function(m){console.log(m)});
            if (!lang){
              act.val(act.val().trim() + ' $$s ');
            } else {
              if (lang[3] == undefined)
                if (act.val().substr(4,7) == 'ALL:001')
                  act.val(lang[1] + ' $$s ' + lang[2] + ' $$s ');
                else
                  act.val(lang[1] + '$$s ');
              else
                if (act.val().substr(4,7) == 'ALL:001')
                  act.val(lang[1] + lang[3] + ' $$s ' + lang[2] + ' $$s ');
                else
                  act.val(lang[1] + lang[3] + ' $$s ');
            }
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 77: // Ctrl-Alt-M Ortskürzel -> µ darum mit Shift oder:
      case 192: // Ctrl-Alt-Ö Ortskürzel unter Linux
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            var lang = act.val().match(/(.*?)\$\$o *(.*?) *(\$\$.*)?$/);
            if (!lang){
              act.val(act.val().trim() + ' $$o ');
            } else {
              if (lang[3] == undefined)
                act.val(lang[1] + '$$o ');
              else
                act.val(lang[1] + lang[3] + ' $$o ');
            }
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 78: // Ctrl-Alt-N Namenskürzel
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            var pers = act.val().match(/(.*?)\$\$n *(.*?) *(\$\$.*)?$/);
            if (!pers){
              act.val(act.val().trim() + ' $$n ');
            } else {
              if (pers[3] == undefined)
                act.val(pers[1] + '$$n ');
              else
                act.val(pers[1] + pers[3].trim() + ' $$n ');
            }
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 80: // Ctrl-Alt-P Summenzeile neu berechnen
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            var lastEmpty;
            var tuSysHgs = new Array;
            var alteSys;
            $("[id^='MarcEditorPresenter.textArea.983'] textarea").each(function(i,e){
              var regs;
              debug(':'+e.value.trim()+':',4).then(function(m){console.log(m)});
              if (e.value.trim() == '$$a' || e.value.trim() == '' || !!e.value.match(/^\$\$a( [A-Z]{3}:)* *$/)){
                lastEmpty = e;
              } else {
                if (regs = e.value.match(/\$\$a ([A-Z]{3}):[A-Z0-9]{3}(\)?)/)){
                  debug(regs,4).then(function(m){console.log(m)});
                  tuSysHgs.push(regs[1]+':');
                  if (!!regs[2]) alteSys=e;
                }
              }
            });
            if (!!alteSys){
              $(alteSys).focus().click();
              browser.runtime.sendMessage({
                action: "toast",
                message: "Systematik soll nicht mehr, oder nur mit Zusatzcode verwendet werden! (löschen mit Strg-Alt-Backspace)",
                color: '#ff8f8f',
                duration: 20
              });
            } else if (lastEmpty != undefined){
              $(lastEmpty).parent().parent().get(0).dispatchEvent(click);
              $(lastEmpty).focus().click().val('$$a ' + Array.from(new Set(tuSysHgs)).sort().join(' ')).get(0).dispatchEvent(change);
            }
          }
        }
        break;
      case 81: // Ctrl-Alt-Q (Quer-)Verweis auf andere Hauptgruppen
        if (strgAlt){
          event.stopPropagation();
          event.preventDefault();
          if (event.type == "keydown"){
            var pers = act.val().match(/(.*?)\$\$t *(.*?) *(\$\$.*)?$/);
            if (!pers){
              act.val(act.val().trim() + ' $$t ');
            } else {
              if (pers[3] == undefined)
                act.val(pers[1] + '$$t ');
              else
                act.val(pers[1] + pers[3].trim() + ' $$t ');
            }
            act.trigger('change');
            act.autocomplete('search',act.val());
          }
        }
        break;
      case 73: // Strg-Alt-I isbn/titel konkordanzsuche
        if (strgAlt){
          konksuche = event.key;
          isbn_suche();
        }
        break;
      case 75: // Strg-Alt-K
        // konkordanzsuche
        if (strgAlt){
          konksuche = event.key;
          if (act.val().trim() == '$$a' || act.val().trim() == ''){
            var selection = getKeywords().trim();
            if (selection != '')
              act.autocomplete('search',selection);
            else
              konksuche = '';
          } else
            act.autocomplete('search',act.val());
        }
        break;
    }
  } else if (mde && strgAlt && isTuw && !!settings.tuwsysUrl && event.type == "keyup" && (event.keyCode == 75 || event.keyCode == 73 || event.keyCode == 66)){
    konksuche = event.key;
    debug(konksuche,2).then(function(m){console.log(m)});
    act = $("div[id^='MarcEditorPresenter.textArea.983'] textarea").filter(function(){
      return (this.value.trim() == '' || this.value.trim() == '$$a')
    });
    // debug(act).then(function(m){console.log(m)});
    if (act.length > 0){
      act = act.first();
      if (konksuche == 'i' || konksuche == 'I'){
        isbn_suche();
      } else {
        var selectedTextArea = document.activeElement;
        var selection = selectedTextArea.value.substring(selectedTextArea.selectionStart, selectedTextArea.selectionEnd).trim();
        if (selection.length == 0)
          selection = document.getSelection().toString().trim();
        if (selection.length == 0)
          selection = getKeywords();
        debug(selection,2).then(function(m){console.log(m)});
        if (selection.length > 0)
          act.autocomplete('search',selection);
        else {
          browser.runtime.sendMessage({
            action: "toast",
            message: "Keine Schlagwörter vorhanden, bitte einen Begriff zur Konkordanzsuche selektieren, oder im 983er eingeben!",
            color: '#ffff8f',
            duration: 10
          });
          konksuche = '';
        }
      } 
    } else {
      browser.runtime.sendMessage({
        action: "toast",
        message: "bitte vorher 0_SE-3-SYS Template laden (Strg-Alt-T), bzw. ein paar leere 983er anlegen!",
        color: '#ffff8f',
        duration: 10
      });
      konksuche = '';
    }
  }
}

function tuwsys(){

  window.addEventListener("keyup",    keyscan, true);
  window.addEventListener("keydown",    keyscan, true);
  // window.onKeydown = keyscan;
  window.addEventListener("keypress", keyscan, true);

  if (!!settings.gndUrl && settings.gndUrl != '' || !!settings.lobidUrl && settings.lobidUrl != ''){
    let f100_700 = (!!settings.creatorAC)?", [id^='MarcEditorPresenter.textArea.100'], [id^='MarcEditorPresenter.textArea.700']":'';
    $("[id^='MarcEditorPresenter.textArea.971']"+f100_700).filter(function(){
      let ind1 = $(this).closest('tr').find("[ID^='FieldIndicatorBox.tag'][id$='.1']").val();
      return ind1 == "0" || ind1 == "1" || ind1 == "2";
    }).find('textarea').autocomplete({
      source: function (request, response) {
        var subs = request.term.match(/\$\$[^a].*/); // alles außer $$a
        let search_term = request.term.replace(/\$\$a|\$\$[^a].*|,/g,'').trim();
        if (subs) subs = subs[0].replace(/\$\$0[^\$]*/,'').trim(); // $$0 löschen
        else subs='';
        if (subs != '') subs = ' '+subs.trim();
        if (!!settings.gndUrl && settings.gndUrl != '')
          jQuery.get(settings.gndUrl, {
              version: '1.1',
              operation: 'searchRetrieve',
              query: 'PER='+search_term+' and BBG=Tp*',
              recordSchema: 'MARC21-xml',
              maximumRecords: 100
            },
            function (data) {
              let xml = $(data);
              let rec = xml.find('recordData record');
              var ndata = [];
              if (rec.length > 0){
                rec.each(function(i,r){
                  let e=$(r);
                  let name = e.find('datafield[tag="100"] subfield[code="a"]');
                  let gnd = e.find('controlfield[tag="001"]');
                  if (name.length > 0 && gnd.length > 0){
                    var view = '';
                    var ext = '';
                    e.find('datafield[tag="548"] subfield[code="a"]').each(function(j,d){
                      view += ' '+d.textContent;
                    });
                    var delim = ' ';
                    e.find('datafield[tag="550"] subfield[code="a"]').each(function(j,d){
                      view += delim+d.textContent;
                      delim = '/';
                    });
                    delim = ' ORCID:';
                    e.find('datafield[tag="024"]:has(subfield[code="2"]:contains("orcid")) subfield[code="a"]').each(function(j,d){
                      view += delim+d.textContent;
                      delim = ',';
                    });
                    delim = '<b>Geo-Area:</b> ';
                    e.find('datafield[tag="043"] subfield[code="c"]').each(function(j,d){
                      ext += delim+d.textContent+'<br/>';
                      delim = '&nbsp;&nbsp;';
                    });
                    delim = '<b>Affiliation:</b> ';
                    e.find('datafield[tag="510"] subfield[code="a"]').each(function(j,d){
                      ext += delim+d.textContent+'<br/>';
                      delim = '&nbsp;&nbsp;';
                    });
                    delim = '<b>Bio:</b> ';
                    e.find('datafield[tag="678"] subfield[code="b"]').each(function(j,d){
                      ext += delim+d.textContent+'<br/>';
                      delim = '&nbsp;&nbsp;';
                    });
                    if (ext != '') view += '<div class="tuwsys-tooltip">'+ext+'</div>';
                    ndata.push({
                      value: '$$a '+ name.get(0).textContent.trim() + subs + ' $$0 (DE-588)'+gnd.get(0).textContent.trim(), // $$2 gnd??
                      label: '<b>' + name.get(0).textContent.trim() + '</b>' + view
                    });
                  }
                });
                ndata[0].counts = {limit: ndata.length, count: xml.find('numberOfRecords').get(0).textContent };
                ndata[0].link = "https://portal.dnb.de/opac/moveDown?currentResultId=per%3D"+encodeURIComponent(search_term)+"%26any&categoryId=persons";
              } else
                ndata.push({
                  value: request.term, // $$2 gnd??
                  label: 'nichts gefunden!',
                  counts: {limit: ndata.length, count: xml.find('numberOfRecords').get(0).textContent },
                  link: "https://portal.dnb.de/opac/moveDown?currentResultId=per%3D"+encodeURIComponent(search_term)+"%26any&categoryId=persons"
                })    
              response(ndata);
            }
          ).fail(function(xhr,msg,err){
            console.log(xhr);
          })
        else
          jQuery.get(settings.lobidUrl, {
            q: search_term,
            format: 'json',
            filter: '+(type:Person)',
            size: 100
          },
          function (data) {
            var ndata = [];
            if (data.totalItems > 0)
              data.member.forEach(function(member){
                if (!!member.preferredName && !!member.gndIdentifier)
                  var view = '';
                  var ext = '';
                  if (!!member.dateOfBirth) view += ' '+member.dateOfBirth.join('?')+'-';
                  if (!!member.dateOfDeath) view += ' '+member.dateOfDeath.join('?');
                  if (!!member.academicDegree) view += ' '+member.academicDegree.join(' ');
                  if (!!member.professionOrOccupation) view += ' '+member.professionOrOccupation.map(p => p.label).join('/');
                  if (!!member.geographicAreaCode) ext += '<b>Geo-Area:</b> '+member.geographicAreaCode.map(p => p.label).join('<br/>&nbsp;&nbsp;')+'<br/>';
                  if (!!member.affiliation) ext += '<b>Affiliation:</b> '+member.affiliation.map(p => p.label).join('<br/>&nbsp;&nbsp;')+'<br/>';
                  if (!!member.biographicalOrHistoricalInformation) ext += '<b>Bio:</b> '+member.biographicalOrHistoricalInformation.join('<br/>&nbsp;&nbsp;')+'<br/>';
                  if (ext != '') view += '<div class="tuwsys-tooltip">'+ext+'</div>';
                  ndata.push({
                    value: '$$a '+ member.preferredName.trim() + subs + ' $$0 (DE-588)'+member.gndIdentifier, // $$2 gnd??
                    label: '<b>' + member.preferredName+'</b>'+view
                  });
                  ndata[0].counts = {limit: ndata.length, count: data.totalItems};
                  ndata[0].link = settings.lobidUrl+'?q='+encodeURIComponent(search_term)
                    +'&filter='+encodeURIComponent('+(type:Person)');
              })
            else
              ndata.push({
                value: request.term, // $$2 gnd??
                label: 'nichts gefunden!',
                counts: {limit: 0, count: 0},
                link: settings.lobidUrl+'?q='+encodeURIComponent(search_term)
                  +'&filter='+encodeURIComponent('+(type:Person)')
              });
            response(ndata);
          }).fail(function(xhr,msg,err){
            console.log(xhr);
          })
      },
      change: function (){
        this.dispatchEvent(change1);
        // this.dispatchEvent(input);
        // $(this).trigger("change");
      },
      classes: { "ui-autocomplete": "tulama-tuwsys" },
      html: true,
      position: { my: "left top", at: "right bottom", collision: "fit" }
    })
  }

  if (isTuw){
    $("[id^='MarcEditorPresenter.textArea.971'] textarea").filter(function(){
      let ind1 = $(this).closest('tr').find("[ID^='FieldIndicatorBox.tag.971'][id$='.1']").val();
      return ind1 == "5";
    }).autocomplete({
      source: function (request, response) {
        let subs = request.term.match(/\$\$a(.*?)(?:\$\$b(.*?))?(?:\$\$c(.*?))?(?:\$\$d(.*?))?(?:\$\$0(.*?))?(?:\$\$e(.*?))?$/);
        if (!!subs[4] && subs[4].trim() != '')
          search_term = subs[4].trim();
        else if (!!subs[3] && subs[3].trim() != '')
          search_term = subs[3].trim();
        else if (!!subs[2] && subs[2].trim() != '')
          search_term = subs[2].trim();
        else if (!!subs[1] && subs[1].trim() != '')
          search_term = subs[1].trim();
        else
          search_term = 'Fakultät';
        jQuery.get('https://tiss.tuwien.ac.at/api/orgunit/v22/osuche', {
          q: search_term,
          max_treffer: 100
        },
        function (data) {
          var ndata = [];
          data.results.forEach(function(res){
            if (res.type == 'INS' || res.type == 'ABT' || res.type == 'OOG' || res.type == 'GKI' || res.type == 'EXT')
              ndata.push({
                value: '$$a Technische Universität Wien $$b ' + res.parent_org_ref.name_de + ' $$c ' + res.name_de + ' $$d '+ res.code,
                label: '<b>' + res.code + ' - ' + res.name_de + '</b>',
                childs: res.child_orgs_refs,
                base: '$$a Technische Universität Wien $$b ' + res.parent_org_ref.name_de + ' $$c ' + res.name_de
              });
            else if (res.type == 'FAK' || res.type == 'VIR' || res.type == 'REK' || res.type == 'GRU')
              ndata.push({
                value: '$$a Technische Universität Wien $$b ' + res.name_de + ' $$d '+ res.code,
                label: '<b>' + res.code + ' - ' + res.name_de + '</b>',
                childs: res.child_orgs_refs,
                base: '$$a Technische Universität Wien $$b ' + res.name_de
              });
          });
          if (ndata.length == 1){
            ndata[0].childs.forEach(function(res){
              if (res.type == 'INS' || res.type == 'ABT' || res.type == 'OOG' || res.type == 'GKI' || res.type == 'EXT')
                ndata.push({
                  value: ndata[0].base + ' $$c ' + res.name_de + ' $$d '+ res.code,
                  label: '&nbsp;&nbsp;<b>' + res.code + ' - ' + res.name_de + '</b>'
                });
              else if (res.type == 'FOB' || res.type == 'FAB' || res.type == 'DEK')
              ndata.push({
                value: ndata[0].base + ' $$d '+ res.code + ' $$e ' + res.name_de,
                label: '&nbsp;&nbsp;<b>' + res.code + ' - ' + res.name_de + '</b>'
              });
            });
          }
          if (ndata.length > 0){
            ndata[0].counts = { limit: ndata.length, count: data.total_results };
            ndata[0].link = "https://tiss.tuwien.ac.at" + data.query_uri
          } else {
            ndata.push({
              value: request.term, // $$2 gnd??
              label: 'nichts gefunden!',
              counts: {limit: 0, count: data.total_results},
              link: "https://tiss.tuwien.ac.at" + data.query_uri
            });
          }
          response(ndata);
        }).fail(function(xhr,msg,err){
          console.log(xhr);
        })
      },
      change: function (){
        this.dispatchEvent(change1);
        // this.dispatchEvent(input);
        // $(this).trigger("change");
      },
      classes: { "ui-autocomplete": "tulama-tuwsys" },
      html: true,
      position: { my: "left top", at: "right bottom", collision: "fit" }
    })
  }

  if (isTuw && !!settings.tuwsysUrl && settings.tuwsysUrl != ''){
    var selected=false;
    var item = false;

    function copy_tuwsys(tuwsys){
      // debug(tuwsys).then(function(m){console.log(m)});
      var e983 = $("div[id^='MarcEditorPresenter.textArea.983'] textarea").filter(function(){
        return (this.value.trim() == '' || this.value.trim() == '$$a')
      });
      if (e983.length < tuwsys.length){
        browser.runtime.sendMessage({
          action: "toast",
          message: "zu wenig leere 983er Felder (" + e983.length + " < " + tuwsys.length + ")!",
          color: '#ffff8f',
          duration: 20
        });
      } else {
        tuwsys.forEach(function(ts,i){
          e983[i].value = ts.value;
          e983[i].dispatchEvent(change);
          e983[i].dispatchEvent(input);
          $(e983[i]).blur().focus();
        });
      }
    }

    $("[id^='MarcEditorPresenter.textArea.983'] textarea").filter(function(){
      return !(this.value.includes('AT-OBV') || this.value.includes('$$z'));
    }).not(':last').autocomplete({
      source: function (request, response) {
        debug(request.term.substr(-4).trim(),2).then(function(m){console.log(m)});
        if (request.term.substr(-4).trim() == '$$n'){
          // Personen und Körperschaften aus der Verschlagwortung
          var pbs = new Array();
          $("[id^='MarcEditorPresenter.textArea.689'] textarea, [id^='MarcEditorPresenter.textArea.600'] textarea, [id^='MarcEditorPresenter.textArea.610'] textarea").filter(function(){
            return this.parentNode.parentNode.id.startsWith('MarcEditorPresenter.textArea.610')
                || this.parentNode.parentNode.id.startsWith('MarcEditorPresenter.textArea.600')
                || !!this.value.match(/\$\$D [bp]/);
          }).each(function(){
            var gnd = this.value.match(/\$\$a *(.*?) *(\$\$|$)/);
            var tus = request.term.trim().match(/\$\$a *(...:...)( .*?) *(\$\$.*|$)/);
            debug(tus,2).then(function(m){console.log(m)});
            pbs.push({
              'value': '$$a ' + tus[1] + ' ' + gnd[1].substr(0,3).toUpperCase() + ' ' + tus[3] + ' ' + gnd[1],
              'label': '<b>' + this.parentNode.parentNode.id.substr(-3) + '</b> ' + $('<span/>').text(this.value.substr(4).replace(/\$\$/g,'|')).html()
            });
          });
          // doppelte einträge löschen
          response(pbs.filter(function (value, index, self){ 
            var fi = self.findIndex(function(val){
              return val.value === value.value;
            });
            return index === fi;
          }));
        } else if (request.term.substr(-4).trim() == '$$o'){
          // Geographikum
          var geos = new Array();
          var tus = request.term.trim().match(/\$\$a *(.*?) (.{3})(:.{3})?( (.{3})?(:.{3})? *)(\$\$.*)$/);
          debug(tus,2).then(function(m){console.log(m)});
          if (tus[1] == ''){
            tus[1] = tus[2] + tus[3];
            tus[2] = '---';
          }
          $("[id^='MarcEditorPresenter.textArea.689'] textarea, [id^='MarcEditorPresenter.textArea.651'] textarea, [id^='MarcEditorPresenter.textArea.662'] textarea").filter(function(){
            return this.parentNode.parentNode.id.startsWith('MarcEditorPresenter.textArea.651')
                || this.parentNode.parentNode.id.startsWith('MarcEditorPresenter.textArea.662')
                || !!this.value.match(/\$\$D g/);
          }).each(function(){
            var gnd = this.value.match(/\$\$a *(.*?)( \$\$.*)?$/);
            var gndnr = gnd[2].match(/\$\$0 *\(DE-588\)(.*?)( \$\$|$)/);
            debug(gnd,2).then(function(m){console.log(m)});
            geos.push({
              'value': '$$a ' + tus[1] + ' ' + tus[2] + ':' + gnd[1].substr(0,3).toUpperCase() + tus[4] + tus[7].trim() + ' ' + gnd[1],
              'label': '<b>' + this.parentNode.parentNode.id.substr(-3) + '</b> ' + $('<span/>').text(this.value.substr(4).replace(/\$\$/g,'|')).html(),
              'gndnr': !!gndnr?gndnr[1]:undefined
            });
          });
          // doppelte einträge löschen
          response(geos.filter(function (value, index, self){   
            return index === self.findIndex(function(val){
              return val.value === value.value;
            });
          }));
        } else {
          // debug(request.term).then(function(m){console.log(m)});
          // debug(this).then(function(m){console.log(m)});
          jQuery.post(settings.tuwsysUrl, {
            QUERY: request.term,
            TIT: $("[id^='MarcEditorPresenter.textArea.245'] textarea").val(),
            KONK: konksuche,
            MMSID: $("[id^='MarcEditorPresenter.textArea.001'] textarea").val(),
            CURPOS: this.element.get(0).selectionStart
          },
          function (data) {
            // debug(data).then(function(m){console.log(m)});
            if (konksuche != '' && data.length == 0){
              browser.runtime.sendMessage({
                action: "toast",
                message: konksuche + " suche nach »" + request.term + "« hat nichts ergeben",
                color: '#ffff8f',
                duration: 10
              });
              konksuche = '';
            }
            if (data.length > 0 && data[0].tuwsys != undefined){
              var ndata = new Array;
              var bk = '';
              var sws = '';
              data.forEach(function(item){
                if (!!item.bk)
                  bk = '<br/>' + item.bk.replace(/\n/g,'<br/>');
                else
                  bk = '';
                if (!!item.sws)
                  sws = item.sws.replace(/\n/g,'<br/>');
                else
                  sws = '';
                ndata.push({label: '<b>'+item.ac+'</b><p class="sub">'+ (item.title+' / '+item.autor).replace(/</g,'&lt;').replace(/>/g,'&gt;') + '<br/>' + sws + bk + '</p> ', value: item.tusyss, sub: item.tuwsys});
                if (!!data[0].limit) ndata[0].counts = {limit: data[0].limit, count: data[0].count, titles: data[0].titles };
                if (!!data[0].link) ndata[0].link = data[0].link;
              });
              response(ndata);
            } else
              response(data);
          });
        }
      },
      select: function( event, ui ) {
        debug(ui,4).then(function(m){console.log(m)});
        debug(event,4).then(function(m){console.log(m)});
        item = ui.item
        if (ui.item.value != undefined){
          selected = ui.item.value.match(/\$\$a *(.*?) *(\$\$.*|$)/);
          debug(selected,4).then(function(m){console.log(m)});
        }
      },
      close:  function( event, ui ) {
        if (!!item && item.sub != undefined){
          if (this.value.substr(0,3) != '$$a')
            this.value = '$$a ';
          copy_tuwsys(item.sub);
        } else if (!!item && item.gndnr != undefined && this.value.substr(12,3) == '---'){
          var that = this;
          jQuery.post(settings.tuwsysUrl, {
            QUERY: item.gndnr,
            KONK: 'G'
          },
          function(data){
            var cur = that.value.match(/\$\$a *(.*?) *(\$\$g .*?)? *(\$\$o .*)?( *\$\$[^dego].*?)?$/);
            if (cur && data[0] != undefined){
              if (cur[4] == undefined) cur[4] = '';
              if (data[0].code.length == 3){
                if (cur[1].substr(0,3) == 'ALL') data[0].code += ':---';
                that.value = "$$a " + cur[1].substr(0,8) + data[0].code + cur[1].substr(15) + " $$g " + data[0].g + cur[4];
              } else
                that.value = "$$a " + cur[1].substr(0,8) + data[0].code + cur[1].substr(15) + " $$g " + data[0].g + " $$o " + data[0].o + cur[4];
              that.dispatchEvent(change);
              $(that).trigger("change");
            } else {
              debug(that,2).then(function(m){console.log(m)});
              debug(data,2).then(function(m){console.log(m)});
            }
          });
        } else {
          this.dispatchEvent(change);
          $(this).trigger("change");
          if (!!selected && (selected[1].length < 8 && (selected[1][3] == ':' || selected[1].trim().length < 4) || selected[0].substr(-4).trim().substr(0,2) == '$$')){
            $(this).autocomplete('search',selected[0]);
          }
        }
        item = false;
        selected=false;
        konksuche = '';
      },
      open: function(){
        $(this).autocomplete("widget").find('li.autocomplete_title a').on('click',function(){
          konkWin = window.open(this.href,this.target);
          // console.log(konkWin);
          return false;
        });
        debug('ac opened',2).then(function(m){console.log(m)});
      },
      classes: { "ui-autocomplete": "tulama-tuwsys" },
      html: true,
      position: { my: "left top", at: "right bottom", collision: "fit" }
    });
  }
}

var last_mmsid;

function habe_fertig(){
  last_mmsid = false;
  debug("habe fertig").then(function(m){console.log(m)});
  if (!!document.getElementById('tu_lAma_blocker')) return;
  addMDEColors();
  gotoFirstEmpty();
  tuwsys();
  checkbug00576546();
}

function check852c(){
  var change = document.createEvent('HTMLEvents');
  change.initEvent('keyup', false, true);
  change.code = 37;
  change.keyCode = 37;
  change.which = 37;
  var f852_8 = $("[id^='MarcEditorPresenter.textArea.852'] textarea");
  if (f852_8.length == 0)
    f852_8 = $('iframe#yards_iframe').contents().find("[id^='MarcEditorPresenter.textArea.852'] textarea");
  var re = /^(.*)(\$\$c\s*)(.*?)(\$\$.*|$)/;
  var match = re.exec(f852_8.val());
  if (!!match && match[3] == ''){
    f852_8.val(match[1].trim() + ' $$c ' + settings.add852c + ' ' + match[4]);
    f852_8.click().focus().get(0).dispatchEvent(change);
    return true;
  }
  return false;
}

// alter MDE
function catlevel(){
  
  function check852c(e){
    var change = document.createEvent('HTMLEvents');
    change.initEvent('keyup', false, true);
    change.code = 37;
    change.keyCode = 37;
    change.which = 37;
    var f852_8 = $("[id^='MarcEditorPresenter.textArea.852'] textarea");
    var re = /^(.*)(\$\$c\s+)(.*?)(\$\$.*|$)/;
    var match = re.exec(f852_8.val());
    if (!!match && match[3] == ''){
      f852_8.val(match[1].trim() + ' $$c ' + settings.add852c + ' ' + match[4]);
      f852_8.click().focus().get(0).dispatchEvent(change);
      f852_8.blur();
    }
    e.target.onblur(e);
  }

  recordLoaded(); // beim direkten öffnen ist der record hier schon geladen
  var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      debug(mutation,4).then(function(m){console.log(m)});
      if (mutation.addedNodes.length >= 1){
        if ($(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.deleteRecordImage)')){
          recordLoaded();
        }
        if ($(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.addItemToHoldingImage)')
         || $(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.saveRecordImage)')){
          if (!!settings.add852c){
            mutation.target.onclick = check852c;
          }
        }
      }
    });
  });
  // das sollte die menueleiste sein, bei jedem neuen record werden am ende zumindest teile neu geladen
  debug($('table.sectionContainer').closest('tr').prev().get(0),3).then(function(m){console.log(m)});
  observer.observe($('table.sectionContainer').closest('tr').prev().get(0),{childList:true,subtree:true});
  // manchmal ist der save button von anfang an da
  if (isTuw && settings.add852c == undefined || !!settings.add852c){
    shelfLoc = (settings.add852c == undefined)?'UNASSIGNED':settings.add852c;
    $('table.sectionContainer').closest('tr').prev()
      .find('div.html-face:has(i#MenuIconBar\\.saveRecordImage),div.html-face:has(i#MenuIconBar\\.addItemToHoldingImage)')
      .each(function(i,e){
        e.parentNode.onclick = check852c;
      });
  }

  // beim öffnen, änderungen von tags oder indikatoren wird alles zweimal neu aufgebaut tag für tag
  // beim ersten mal merken wir uns das letzte tag (inkl anzahl)
  // beim zweiten durchlauf wissen wir dann beim letzten tag dass wir fertig sind...  
  var tag;
  var cur_mms_id;
  var highest_tag
  var last_tag;
  var nr_last_tag;
  var nr_highest_tag;
  var timer;
  var robserver = new MutationObserver(function(mutations) {
    // console.log(document.getElementById('tu_lAma_blocker'));
    mutations.forEach(function(mutation) {
      // debug(mutation.addedNodes).then(function(m){console.log(m)});
      if (mutation.addedNodes.length >= 1 && $(mutation.addedNodes[0]).is("div[id^='MarcEditorPresenter.textArea']")){
        tag = mutation.addedNodes[0].id.substr(-3);
        if (tag == '001'){
          clearTimeout(timer);
          timer = setTimeout(habe_fertig,500);
          cur_mms_id = $(mutation.addedNodes[0]).find('textarea').val();
          if (cur_mms_id != last_mmsid){
            last_mmsid = cur_mms_id;
            highest_tag = undefined;
            last_tag = '001';
          } else {
            highest_tag = last_tag;
            nr_highest_tag = nr_last_tag;
          }
        } else if (tag != 'LDR'){
          clearTimeout(timer);
          if (last_tag != tag)
            nr_last_tag = 1;
          else
            nr_last_tag++;
          last_tag = tag;
          if (tag == highest_tag){
            if (nr_highest_tag == 1){
              habe_fertig();
            } else {
              nr_highest_tag--;
              timer = setTimeout(habe_fertig,500);
            }
          } else {
            timer = setTimeout(habe_fertig,500);
          }
        }
        debug(last_mmsid + ' ' + tag + ' ' + cur_mms_id +  ' ' + last_tag +  ' ' + nr_highest_tag + ':' + highest_tag, 3).then(function(m){console.log(m)});
      }
    });
  });
  robserver.observe($('table.sectionContainer').closest('tbody').get(0),{childList:true,subtree:true});
}

function checkbug00576546(){
  // MD_Editor wird geladen ein Datensatz ist schon geöffnet und er ist in einem Set
  // frage ob bei allen records in dem set der Ursprünglich Datensatz geladen werden soll -> lokale felder wieder da
  // sollte also nur kommen wenn man ein set zum katalogisieren neu geöffnet hat
  if (isTuw && !settings.checkBug00576546){
    if ($('#FieldTagBox\\.tag\\.001').length > 0){
      var mmsid = $("div[ID^='MarcEditorPresenter.textArea.001'] textarea").val();
      if (newMde)
        var locfs = $('table[id^="editorTable"] tr:has(table td.localFieldIndication) input[id^="FieldTagBox.tag"]').map(function() { return this.id.substr(-3); }).get().join(' ');
      else
        var locfs = $('td.localFieldIndication input').map(function() { return this.id.substr(-3); }).get().join(' ');
      debug(mmsid + ': ' + locfs,2).then(function(m){console.log(m)});
      $.post("https://almagw.ub.tuwien.ac.at/tulama/lfs.php",{MMSID: mmsid}, function(data){
        if (data.local_fields == undefined){
          browser.runtime.sendMessage({
            action: "toast",
            message: locfs + ' aktuell geladenen lokale Felder. Keine ältere Version zum Vergleichen gefunden!',
            color: '#ffff8f',
            duration: 20
          });
        } else if (data.local_fields == locfs){
          if (locfs != ''){
            browser.runtime.sendMessage({
              action: "toast", 
              message: 'Lokale Felder i.o.: ' + locfs,
              color: '#8fff8f',
              duration: 5
            });
          } else {
            browser.runtime.sendMessage({
              action: "toast", 
              message: 'Keine lokalen Felder, schaut aber OK aus.',
              color: '#8fff8f',
              duration: 5
            });
          }
        } else if (locfs == ''){
          browser.runtime.sendMessage({
            action: "toast", 
            message: '<b>Achtung keine lokalen Felder! Ursprünglichen Datensatz neuladen.</b><br>Letzte bekannte Version vom ' + data.file_mod + ': ' + data.local_fields,
            color: '#ff8f8f',
            duration: 20
          });
        } else if (data.local_fields == ''){
          browser.runtime.sendMessage({
            action: "toast",
            message: locfs + ' Lokale Felder aktuell.<br>' + 'Noch keine lokalen Felder in der Version vom ' + data.file_mod,
            color: '#8fff8f',
            duration: 5
          });
        } else {
          browser.runtime.sendMessage({
            action: "toast", 
            message: locfs + ' Unterschiedliche lokale Felder! Aktuell.<br>'+ data.local_fields +' Letzte bekannte Version vom ' + data.file_mod,
            color: '#ffff8f',
            duration: 20
          });
        }
      },"json").fail(function(jqXHR, textStatus, errorThrown) {
        debug(textStatus,2).then(function(m){console.log(m)});
      });
    }
  }
}

function sort_items(by){ // Description
  $("#pageBeansortSectionPropertiesroutine_hiddenSelect option[value='"+by+"']").prop('selected',true).trigger('change');
  setTimeout(function(){
    $('#widgetId_Left_sortroutine').removeClass('open');
    $('#TABLE_DATA_list').find("span[id$='_COL_arrivalDate'][title!='-']").get(0).scrollIntoView(false);
    window.scrollBy(0,200);
  },1000);
}

function expandSearchValues(dummy){
  if ($('#advancedSearchWrapper:hidden').length == 1){
    $('#advancedLink').get(0).dispatchEvent(click);
    setTimeout(function(){$('#recentSearchesContainer').removeClass('open');},1000);
  } else if ($('.advSearchValueWrapper input').length == 1){
    var list = $('.advSearchValueWrapper input').val().replace(/[\s;,\|]/g,' ').split(' ');
    list = list.filter(function(val){ return val.trim() != ''; });
    if (list.length > 100 && !confirm('Wirklich ' + list.length + ' Werte expandieren?')) return;
    debug(list,2).then(function(m){console.log(m)});
    list.forEach(function(val, i){
      $('.advSearchValueWrapper input').last().val(val);
      if (list.length > i+1){
        $('.duplicateRowBtn').last().get(0).dispatchEvent(click);
        $('.advAndOrWrapper input[value="OR"]').last().click().get(0).dispatchEvent(click);
      }
    });
    setTimeout(function(){ $('#advSearchBtn').get(0).scrollIntoView(false); window.scrollBy(0, 100); },500);
    browser.runtime.sendMessage({
      action: "toast",
      message: list.length + ' Werte!',
      color: '#8fff8f',
      duration: 20
    });
  } else {
    browser.runtime.sendMessage({
      action: "toast",
      message: 'Bitte das Formular vorher löschen!',
      color: '#ffff8f',
      duration: 20
    });
  }
}

function add_loans(what){
  if ((isTuw && !!settings.loansUrl) || document.URL.includes('ubtuw')){
    if (!settings.loansUrl) settings.loansUrl = 'https://almagw.ub.tuwien.ac.at/tulama/loans.php';
    if (what == 'itemlist'){
      var physiids = $("input[name='pageBean.selectedResultsList.values'][type='checkbox']").map(function() { return this.value; }).get();
      if (physiids.length == 0){
        toast('keine Item-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'ITEMID[]': physiids
          },
          function(data){
            $('#SELENIUM_ID_list_HEADER_accessionNumber').text('Entl./heuer/letzte');
            data.forEach(function(val){
              $("#recordContainerlist" + val.item_id + " span[id$='COL_accessionNumber']").text(val.loans + '/' + val.loans_this_year + '/' + val.last_loan);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    } else if (what == 'holdinglist'){
      var holids = $("input[name='pageBean.selectedResultsList.values'][type='checkbox']").map(function() { return this.value; }).get();
      if (holids.length == 0){
        toast('keine Holding-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'HOLID[]': holids
          },
          function(data){
            $('#SELENIUM_ID_listWithFilters_HEADER_accessionNumber input[type="submit"]').val('Entl./heuer/letzte');
            data.forEach(function(val){
              $("#recordContainerlistWithFilters" + val.hol_id + " span[id$='COL_accessionNumber']").text(val.loans + '/' + val.loans_this_year + '/' + val.last_loan );
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    } else if (what == 'mmsresults'){
      var mms_ids = $('[id^="recordContainerresults"]').map(function() { return this.id.substr(22); }).get();
      if (mms_ids.length == 0){
        toast('keine MMS-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'MMSID[]': mms_ids
          },
          function(data){
            data.forEach(function(val){
              $("#recordContainerresults" + val.mms_id + " .fieldColumn1 div.row").append(`
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen heuer:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans_this_year}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Letzte Entlehnung:</span>
                <span class="spacer_after_1em" dir="auto">${val.last_loan}</span>
              </div>`);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    } else if (what == 'holdingresults'){
      var mms_ids = $('[id$="mmsId"]').map(function() { return this.title; }).get();
      if (mms_ids.length == 0){
        toast('keine MMS-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'MMSID[]': mms_ids
          },
          function(data){
            data.forEach(function(val){
              $(".fieldColumn2:has([title='" + val.mms_id + "']) div.row").append(`
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen heuer:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans_this_year}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Letzte Entlehnung:</span>
                <span class="spacer_after_1em" dir="auto">${val.last_loan}</span>
              </div>`);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    } else if (what == 'itemresults'){
      var item_ids = $('[id^="recordContainerresults"]').map(function() { return this.id.substr(22); }).get();
      if (item_ids.length == 0){
        toast('keine Item-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'ITEMID[]': item_ids
          },
          function(data){
            data.forEach(function(val){
              $("#recordContainerresults" + val.item_id + " .fieldColumn2 div.row").append(`
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Entlehnungen heuer:</span>
                <span class="spacer_after_1em" dir="auto">${val.loans_this_year}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Letzte Entlehnung:</span>
                <span class="spacer_after_1em" dir="auto">${val.last_loan}</span>
              </div>
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Datensatznummer:</span>
                <span class="spacer_after_1em" dir="auto">${val.acnumber}</span>
              </div>`);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    } else if (what == 'portfresults'){
      var mms_ids = $('[id$="LABEL_mmsIdmmsId"]').map(function() { return this.textContent; }).get();
      if (mms_ids.length == 0){
        toast('keine MMS-IDs gefunden','#ff8f8f',10)
      } else {
        $.post(settings.loansUrl,
          {
            'MMSID[]': mms_ids
          },
          function(data){
            data.forEach(function(val){
              $('.fieldColumn2 div.row:has(span[id$="LABEL_mmsIdmmsId"]:contains("'+val.mms_id+'"))').append(`
              <div class="col col-xs-12 marTopBottom3">
                <span class="fieldName">Datensatznummer:</span>
                <span class="spacer_after_1em" dir="auto">${val.acnumber}</span>
              </div>`);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    }
  }
}

function handleKeyEvent(e){
  var oe = e.originalEvent;
  debug(e,2).then(function(m){console.log(m)});
  if ((oe.key != 's' && oe.key != 'i' && (oe.altKey || oe.metaKey) && oe.ctrlKey
      || oe.key == 'Escape'
      || oe.ctrlKey && oe.key == 's'
      || oe.ctrlKey && (oe.altKey || oe.metaKey) && oe.key == 'r'
      || (oe.altKey || oe.metaKey) && !oe.ctrlKey && oe.key == 'i')
    && e.data[oe.key] != undefined)
    e.data[oe.key].action(e.data[oe.key].value);
}

function enable_key_shortcuts(){
  $(document).unbind('keydown.editPhysicalItem');
  $(document).unbind('keydown.PhysicalItemList');
  $(document).unbind('keydown.expandSearchValues');
  $(document).unbind('keydown.MmsResults');
  $(document).unbind('keydown.PhysicalTitleResults');
  $(document).unbind('keydown.PhysicalItemResults');
  $(document).unbind('keydown.ePortfolioResults');
  var kdevent = false;
  var keys = {
    y:{ value: 'expandSearchValues',
        action: expandSearchValues
      },
    L:{ value:  1,
        action: lamaTwinkle
      }
  };
  if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.item_general.xml'){
    kdevent = 'keydown.editPhysicalItem';
    keys.b = { value: 'GG', action: protype };
    keys.B = { value: 'dummy', action: proTypeSettings };
    keys.p = { value: 'Null', action: set_item_policy };
    keys.P = { value: 'Null', action: itemPolicySettings };
    keys.t = { value: 'dummy', action: setItemTemplate };
    keys.T = { value: 'dummy', action: itemTemplateSettings };
    debug('add keydown event_handler for "edit physical item"',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.items_list.xml'){
    kdevent = 'keydown.physicalItemList';
    keys.d = { value: 'Description', action: sort_items };
    if (isTuw){
      keys.l = { value: 'itemlist', action: add_loans };
      // keys.L = { value: 'itemlist', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.holdings_list.xml'){
    kdevent = 'keydown.physicalHoldingList';
    if (isTuw){
      keys.l = { value: 'holdinglist', action: add_loans };
      // keys.L = { value: 'holdinglist', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_search.mms_results.xml'){
    kdevent = 'keydown.MmsResults';
    if (isTuw){
      keys.l = { value: 'mmsresults', action: add_loans };
      // keys.L = { value: 'mmsresults', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_search.physical_ie_results.xml'){
    kdevent = 'keydown.PhysicalTitleResults';
    if (isTuw){
      keys.l = { value: 'holdingresults', action: add_loans };
      // keys.L = { value: 'holdingresults', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_search.physical_item_results.xml'){
    kdevent = 'keydown.PhysicalItemResults';
    if (isTuw){
      keys.l = { value: 'itemresults', action: add_loans };
      // keys.L = { value: 'itemresults', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_search.electronic_portfolio_results.xml'){
    kdevent = 'keydown.ePortfolioResults';
    if (isTuw){
      keys.l = { value: 'portfresults', action: add_loans };
      // keys.L = { value: 'itemresults', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('#persistentSearchWrapper').length == 1){
    kdevent = 'keydown.expandSearchValues';
    debug('add keydown event_handler for "expandSearchValues"',2).then(function(m){console.log(m)});
  }
  debug(kdevent,2).then(function(m){console.log(m)});
  debug(keys,2).then(function(m){console.log(m)});
  if (!!kdevent){
    $(document).bind(
      kdevent,
      keys,
      function(e) {
        handleKeyEvent(e);
      }
    );
  }
}

function addColorTemplate(){
  
  [0,1,2,3,4,5].forEach(function(level){$('body').removeClass('tu_lAma_Warning'+level);});
  try {
    settings.warningList.forEach(function(v){
      // keine andere möglichkeit im simple-view bib und hol zu unterscheiden?
      if (!v.hol && !v.bib || !!v.bib && $('#mmsRecordSimpleViewlabelmarc_span').length > 0 || !!v.hol && $('#mmsRecordSimpleViewlabelmarc_span').length == 0){
        let dubl = $('#TABLE_DATA_marcFieldsList tr:has(span[title="'+v.tag+'"]) td[id$="_COL_value"]').filter(function(){
          return (!v.ind1 || this.textContent.substr(0,1) == v.ind1) 
            && (!v.ind2 || this.textContent.substr(1,1) == v.ind2)
            && (!v.not && this.textContent.match(v.re) != null || !!v.not && this.textContent.match(v.re) == null)
        });
        if (dubl.length > 0){
          $('body').addClass('tu_lAma_Warning'+v.level);
          if (v.level <= 2){
            dubl.closest('tr').addClass('tu_lAma_Warning'+v.level);
            if (!!v.color) dubl.closest('tr').css('background-color', v.color).find('td, th').css('background-color', v.color);
          }
          var color = (!!v.color)?` style="color: ${v.color} !important;"`:'';
          $('.upperActionsLeft .tableSearch').append($(`<span class="tu_lAma_Warning${v.level}"><h2${color}>${v.msg}</h2></span>`));
        }
      }
    });
  } catch(err){ console.log(err) }

  if (settings.colorScheme == undefined) settings.colorScheme = 'rgb';
  if (!!settings.colorScheme){
    var re = /^(..)? (\|. )$/;
    $("td[id$='_COL_value']").contents().each(function(i,n){
      if (n.nodeName=='#text' && (match=re.exec(n.data))){
        if (match[1] == undefined)
          $(n).replaceWith('<span class="subField">' + n.data + '</span>');
        else
          $(n).replaceWith('<span class="indicator">' + match[1] + '</span> <span class="subField">' + match[2] + '</span>');
      }
    });
  }

  if (!settings.hideSH){
    $('body').off('click.extlink');
    $('body').on('click.extlink','#TABLE_DATA_marcFieldsList tr span[title="995"]',function(e){
      let match = e.target.parentNode.nextSibling.textContent.match(/\|u\s*(https?:[^\s]*)/);
      if (match) window.open(match[1],'_blank');
    });
  }
}

function checkMDEWarnings(warningList){
  [0,1,2,3,4,5].forEach(function(level){$('.marcEditor.mainContainer').removeClass('tu_lAma_Warning'+level);});
  warningList.forEach(function(v){
    var bibhol = '';
    if (!!v.bib && !v.hol)
      bibhol = "table.editorTable[lama_type='bib'] ";
    else if (!!v.hol && !v.bib)
      bibhol = "table.editorTable[lama_type='hol'] ";
    var dubl = $(bibhol+"[ID^='MarcEditorPresenter.textArea."+v.tag+"'] textarea").filter(function(){
      var ind1 = $(this).closest('tr').find("[ID^='FieldIndicatorBox.tag."+v.tag+".1']").val();
      var ind2 = $(this).closest('tr').find("[ID^='FieldIndicatorBox.tag."+v.tag+".2']").val();
      return (!v.ind1 || ind1 == v.ind1 || v.ind1 == '_' && ind1 == '')
        && (!v.ind2 || ind2 == v.ind2 || v.ind2 == '_' && ind2 == '')
        && (!v.not && this.value.match(v.re) != null || !!v.not && this.value.match(v.re) == null)
    });
    if (dubl.length > 0){
      dubl.closest('.marcEditor.mainContainer').addClass('tu_lAma_Warning'+v.level);
      if (v.level <= 2){
        dubl.closest('tr').addClass('tu_lAma_Warning'+v.level);
        if (!!v.color) dubl.closest('tr').css('background-color', v.color).find('td, th').css('background-color', v.color);
      }
      browser.runtime.sendMessage({
        action: "toast",
        message: `<h3 style='color: black;'>${v.msg}!</h3>`,
        color: (!!v.color)?v.color:wcols[v.level],
        duration: v.level*5+5
      });
    }
  });
}

function addMDEColors(){
  if (!!document.getElementById('tu_lAma_blocker')) return;
  
  if ($('.alenTagColor').length > 0){
    toast("<h2 style='color: black;'>Bitte das Add-On 'AlEn' deaktivieren bzw. entfernen</h2><h3 style='margin-bottom: 30px; margin-top: 10px; color: black;'>Es ist mit manchen Bookmarklets inkompatibel und unser lAma hat nun eigene lustige Farben</h3>", "orange", 20);
  }
  checkMDEWarnings(settings.warningList);

  if (settings.colorScheme == undefined) settings.colorScheme = 'rgb';
  if (!!settings.colorScheme){
    $('[id^="MarcEditorPresenter.textArea"] div:has("#titleFieldLabelId")').show().on("click focus",function(e){
      debug("clicked or focus",2).then(function(m){console.log(m)});
      debug(e,4).then(function(m){console.log(m)});
      var sel = window.getSelection();
      var selStart=0;
      var selEnd = 0;
      var startFound=false;
      var endFound = false;
      var selDir = "forward";
      $(sel.anchorNode).closest('div').get(0).childNodes.forEach(function(n){
        if (n.contains(sel.anchorNode)){
          selStart += sel.anchorOffset;
          startFound = true;
        }
        if (!startFound)
          selStart += n.textContent.length;
        if (n.contains(sel.focusNode)){
          selEnd += sel.focusOffset;
          endFound = true;
        }
        if (!endFound)
          selEnd += n.textContent.length;
      });
      if (selEnd < selStart){
        selDir = selStart;
        selStart = selEnd;
        selEnd = selDir;
        selDir = "backward";
      }
      // set focus to correspondending textarea and set selection
      var textarea = $(this).parent().find("textarea");
      if (selStart == selEnd && (textarea.val().length == 3 && textarea.val().substr(0,2) == '$$'
          || textarea.val().length == 4 && textarea.val().substr(0,2) == '$$' && textarea.val().substr(3,1) == ' ')){
        if (textarea.val().length == 3) textarea.val(textarea.val()+' ');
        selStart = selEnd = 4;
      }
      textarea.closest('tr').addClass('selectedRow');
      textarea.removeClass('FieldTextAreaReadOnly').focus();
      if (selStart != 0 || selEnd != 0)
        textarea.get(0).setSelectionRange(selStart,selEnd,selDir);
    }).find("#titleFieldLabelId").contents().each(function(i,n){
      if (n.nodeName=='#text')
        $(n).replaceWith($('<div/>').text(n.data).html().replace(/(\$\$.) /g,'<span class="subField">$1</span> '));
    });
    $("[id^='MarcEditorPresenter.textArea'] div:has('textarea')").find('textarea').on("focus click change",function(e){
      debug("focus click change",2).then(function(m){console.log(m)});
      debug(e,4).then(function(m){console.log(m)});
      debug(this,4).then(function(m){console.log(m)});
      if (this.value.length==3 && this.value.substr(0,2) == '$$') this.value += ' ';
    }).on("blur",function(e){
      debug(e,4).then(function(m){console.log(m)});
      debug("blur",4).then(function(m){console.log(m)});
      debug(this,4).then(function(m){console.log(m)});
      $(this).parent().parent().find('div:has("#titleFieldLabelId")').find("#titleFieldLabelId").text(this.value).contents().each(function(i,n){
        if (n.nodeName=='#text')
          $(n).replaceWith($('<div/>').text(n.data).html().replace(/(\$\$.) /g,'<span class="subField">$1</span> '));
      });
    });
  }
}

var diffdiv = false;
let nbsp = String.fromCharCode(160);

function createDiff(){
  
  let i18n = { de: {'left': 'links', 'right': 'rechts' }};

  var rows = $(".showBorderExlibris table[id^='editorTable'] > tbody > tr:has(input[id^='FieldTagBox'])");
  if (rows.length == 0) rows = $("table[id^='editorTable'] > tbody > tr:has(input[id^='FieldTagBox'])");
  var mms_id = rows.find("[id^='MarcEditorPresenter.textArea.001'] textarea").val();
  if (!mms_id) mms_id = rows.closest("table[id^='editorTable']").attr('lama_mms_id');
  let type = rows.closest("table[id^='editorTable']").attr('lama_type');

  function getActiveMarc(rows){
    return rows.map((i,x) => {
      let r = $(x);
      let loc = (r.find('td.localFieldIndication').length > 0)?'L\t':' \t';
      let ind = r.find('input[id^="FieldIndicatorBox"]');
      var inds;
      let sp = ' ';
      if (ind.length == 2){
        inds = ((ind.eq(0).val().length==1)?ind.eq(0).val():sp) + '\t'
             + ((ind.eq(1).val().length==1)?ind.eq(1).val():sp)
      } else {
        inds = sp + '\t' + sp;
      }
      return loc+r.find('input[id^="FieldTagBox"]').val() + '\t' + inds + '\t' + r.find('textarea').val(); 
    }).get().join('\n')
  }

  function getVersion(){
    return $('table.browseRecordTable tr').map((i,x)=>{
      let td = $(x).find('td');
      var i1 = td.eq(1).text();
      if (i1 == '' || i1 == nbsp) i1 = ' ';
      var i2 = td.eq(2).text();
      if (i2 == '' || i2 == nbsp) i2 = ' ';
      return ' \t'+td.eq(0).text() + '\t'+i1+'\t'+i2+'\t'+td.eq(3).text()
    }).get().join('\n')
  }

  function xMarc2tMarc(data, nz_mms_id){
    if (!!nz_mms_id){
      let nz = $(data).find('datafield[tag="035"]:has(subfield[code="a"]:contains("'+nz_mms_id+'"))');
      if (nz.length == 1){
        nz.remove();
        $(data).find('controlfield[tag="001"]').text(nz_mms_id);
      }
    }
    return $(data).find('leader, controlfield, datafield').map((i,x) => {
      let r=$(x);
      var i1 = r.attr('ind1');
      if (!i1 || i1 == ' ') i1 = ' ';
      var i2 = r.attr('ind2');
      if (!i2 || i2 == ' ') i2 = ' ';
      var text;
      var tag;
      var loc = ' \t';
      if (r.is('leader'))
        tag = 'LDR';
      else
        tag = r.attr('tag');
      if (r.is('controlfield') || r.is('leader')) text = r.text().replace(/ /g,'#');
      else {
        text = r.find('subfield').map((j,y) => {
          if ($(y).attr('code') == '9' && $(y).text().toUpperCase() == 'LOCAL'){
            loc = 'L\t';
            return '';
          } else
            return '$$' + $(y).attr('code') + ' ' + $(y).text();
        
        }).get().join(' ');
      }
      let ret = loc + tag + '\t' + i1 + '\t' + i2 + '\t' + text;
      return ret;
    }).get().join('\n');
  }

  function getHolding(holid,url){
    return new Promise(function(resolv, reject){
      let match = document.URL.match(/(https?:\/\/[^\/]*)\//);
      $.ajax({
        url:      match[1] + '/almaws/v1/bibs',
        type:     'GET',
        headers:  { 'X-CloudApp-ID': 'tuwien/lAma' },
        data: { holdings_id: holid, view: 'brief' },
        success:  function(data){
          let mms_id = $(data).find('mms_id').text();
          $.ajax({
            url:      match[1] + '/almaws/v1/bibs/'+mms_id+'/holdings/'+holid,
            type:     'GET',
            headers:  { 'X-CloudApp-ID': 'tuwien/lAma' },
            success:  function(data){resolv(data)},
            error: function(e){reject(e)}
          })
        },
        error: function(e){reject(e)}
      })
    })
  }

  function showDiff(old,act,tit){
    try {
      var dmp = new diff_match_patch();
      dmp.Diff_Timeout = 2;
      dmp.Diff_EditCost = 4;
      var diff = dmp.diff_main(old,act);
      dmp.diff_cleanupEfficiency(diff);
      var diffc = {...diff};
      debug(diffc,5).then((m)=>{console.log(m)});
      diffdiv = $('<div/>');
      for (var i=0; i < diff.length; i++) {
        if (diff[i][0] == -1) {
          if (diff[i+1] != undefined && diff[i+1][0] == 1){
            var t;
            while ((t=diff[i][1].indexOf('\t')) > -1 && diff[i+1][1].indexOf('\t') == t){
              diffdiv.append($('<del/>').html(diff[i][1].substr(0,t)));
              diff[i][1] = diff[i][1].substr(t+1);
              diffdiv.append($('<ins/>').html(diff[i+1][1].substr(0,t)));
              diff[i+1][1] = diff[i+1][1].substr(t+1);
              diffdiv.append(document.createTextNode('\t'));
            }
          }
          if (!!diff[i][1] && diff[i][1] != '\n' && diff[i][1] != '\n ') diffdiv.append($('<del/>').html(diff[i][1]));
        } else if (diff[i][0] == 1) {
          if (!!diff[i][1]) diffdiv.append($('<ins/>').html(diff[i][1]));
        } else {
          if (!!diff[i][1]) diffdiv.append(document.createTextNode(diff[i][1]));
        }
      }
      debug(diffdiv.html().replace(/\n/g,'<br/>\n'),4).then((m)=>{console.log(m)});
      diffdiv.html(diffdiv.html().replace(/\n/g,'<br/>\n').replace(/(\s|>)(\$\$(<[^>]*>)?[a-zA-Z0-9])/g,'$1<sf>$2</sf>')
        .replace(/(\n|^)([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t/g,
          '$1<loc>$2</loc><tag>$3</tag><i1>$4</i1><i2>$5</i2>')
        .replace(/<(del|ins)>([^<]*)<\/(loc|tag|i1|i2|sf)>/g,'<$1>$2</$1></$3><$1>')
        .replace(/<(loc|tag|i1|i2|sf)>([^<]*)<\/(del|ins)>/g,'</$3><$1><$3>$2</$3>'));
    } catch(e) {console.log(e); }
    let dh3 = $('<h3/>').attr('lang','de').text(tit.de);
    let eh3 = $('<h3/>').attr('lang','en').text(tit.en);
    let close = $('<button/>').text('x').addClass('close').on('click',(e)=>{diffdiv.remove(); diffdiv=false; });
    diffdiv.prepend(close);
    diffdiv.prepend(dh3);
    diffdiv.prepend(eh3);
    diffdiv = $('<div/>').attr("id",'tu_lAma_diff_viewer').append(diffdiv);
    diffdiv.prepend($('<div/>').addClass('backdrop'));
    $('body').append(diffdiv);
    diffdiv.focus();
  }
  try {
    let act = getActiveMarc(rows);
    if ($('table.browseRecordTable').length > 0){
      let ver = $('.navigationAndActionsBrowseSearchResults td').eq(4).text();
      let orig = ($('.navigationAndActionsBrowseSearchResults .actionPanel td').length > 0)?'':' original';
      showDiff(getVersion(),act,{ de: 'Diff zu'+orig+' Version '+ ver, en: 'Diff against'+orig+' version '+ver});
    } else if ($(".hideBorderExlibris table[id^='editorTable'] > tbody > tr:has(input[id^='FieldTagBox'])").length > 0
            && $(".hideBorderExlibris table[id^='editorTable']").attr('lama_type') == type) {
      aid = rows.closest('[id^="editorTable"]').attr('id').substr(12);
      daid = i18n.de[aid]?i18n.de[aid]:aid;
      let iact = $(".hideBorderExlibris table[id^='editorTable'] > tbody > tr:has(input[id^='FieldTagBox'])");
      iaid = iact.closest('[id^="editorTable"]').attr('id').substr(12);
      diaid = i18n.de[iaid]?i18n.de[iaid]:iaid;
      showDiff(getActiveMarc(iact),act,{ de: 'Diff zwischen '+daid+' und '+diaid, en: 'Diff of '+aid+' and '+iaid });
    } else {
      let match = document.URL.match(/(https?:\/\/[^\/]*)\//);
      if (type == 'hol'){
        getHolding(mms_id,match[1]).then((xml)=>{
          let old = xMarc2tMarc(xml);
          showDiff(old,act,{ de: 'Diff zur gespeicherten Version', en: 'Diff to saved version'});
        })
      } else {
        $.ajax({
          url:      match[1] + '/almaws/v1/bibs',
          type:     'GET',
          headers:  { 'X-CloudApp-ID': 'tuwien/lAma' },
          data:     { 'nz_mms_id': mms_id },
          success:  function(data){
            let old = xMarc2tMarc(data,mms_id);
            showDiff(old,act,{ de: 'Diff zur gespeicherten Version', en: 'Diff to saved version'});
          },
          error: function(){
            $.ajax({
              url:      match[1] + '/almaws/v1/bibs/'+mms_id,
              type:     'GET',
              headers:  { 'X-CloudApp-ID': 'tuwien/lAma' },
              success:  function(data){
                let old = xMarc2tMarc(data,mms_id);
                showDiff(old,act,{ de: 'Diff zur gespeicherten Version', en: 'Diff to saved version'});
              }
            })
          }
        })
      }
    }
  } catch(e) {console.log(e); }
}

function lamaTwinkle(active = false){
  
  $('body').toggleClass('tu_lAma_lb');
  browser.storage.local.set({ lamaLoadingBlocker: $('body').hasClass('tu_lAma_lb') });

  var nMdeW = $('iframe#yardsNgWrapper');
  if (nMdeW.length > 0){
    if ($('body').hasClass('tu_lAma_lb'))
      nMdeW.contents().find('body').addClass('tu_lAma_lb');
    else
      nMdeW.contents().find('body').removeClass('tu_lAma_lb');
  }
  $('#loadingBlocker').removeClass('hide');
  setTimeout(function(){$('#loadingBlocker').addClass('hide');},1500);
}

function showLaufzettel(e){
  e.preventDefault();
  let barcode = $(this).closest('tr, div.rowsContainer').find('input[id$="barcode"]').val();
  let match = document.URL.match(/(https?:\/\/[^\/]*)\//);
  var popupURL = browser.extension.getURL('templates/processSlip.html?barcode='+encodeURIComponent(barcode)+'&insturl='+encodeURIComponent(match[1]));
  browser.runtime.sendMessage({
    action: "openPopup",
    popupURL: popupURL
  });
  return false;
}

function laufzettel(){
  if (!!settings.processSlip){
    if ($('body').attr('id') == 'body_id_xml_file_loan.fulfillment_patron_workspace_loan_new_ui.xml'
      || $('body').attr('id') == 'body_id_xml_file_resource_editor.physical.items_list.xml'
      || $('body').attr('id') == 'body_id_xml_file_po.poline_receiving_items_list.xml')
      $('td.listActionContainer ul:not(:has(li#processSlip))').append($('<li id="processSlip" class="clearfix"><a class="submitUrl" href="#" title="'+browser.i18n.getMessage('processSlip')+'">'+browser.i18n.getMessage('processSlip')+'</a></li>').on('click',showLaufzettel));
    if ($('body').attr('id') == 'body_id_xml_file_search.physical_item_results.xml')
      $('div.rowActionsContainer ul.internalRowActions:not(:has(li#processSlip))').append($('<li id="processSlip" class="rowAction internalRowAction"><a class="submitUrl" href="#" title="'+browser.i18n.getMessage('processSlip')+'">'+browser.i18n.getMessage('processSlip')+'</a></li>').on('click',showLaufzettel));
  }
}

var initialized;
var newMde = false;
var newMDEWrapper = false;
var newLayout = false;

var mde = false;
var nmdeInit = false;

$(function(){

  if (document.URL.search('exlibrisgroup.com/view/uresolver') === -1 && 
      document.URL.substr(-32) !== 'exlibrisgroup.com/rep/yards.jsp?'){

    // enable tuw features
    if (!!document.URL.includes('obv-at-ubtuw')){
      isTuw = true;
      browser.storage.local.set({ tuwSeen: true });
    }

    browser.storage.local.get().then(function(result){
      settings = result;
      debug(result,3).then(function(m){console.log(m)});

      if (settings.colorScheme == undefined) settings.colorScheme = 'el';

      if (!document.URL.includes('/rep/yards.jsp') && document.URL.substr(-23) != 'alma.exlibrisgroup.com/'){
        if (!!settings.colorScheme && ($('title').text() != 'Md Editor' || document.URL.includes('/rep/MDEditor.jsp'))){
          debug('add class ' + settings.colorScheme + ' to body',2).then(function(m){console.log(m)});
          $('body').addClass('tu_lAma_marc_' + settings.colorScheme);
          if (!document.URL.includes('psb.alma') && !document.URL.includes('sandbox') && (!!settings.overwriteAlmaColors))
            $('body').addClass('tu_lAma_bg_' + settings.colorScheme);
        }
        if (!settings.messageToBottom)
          $('body').addClass('tu_lAma_msg_bottom');
        if (!!settings.messageCut)
          $('body').addClass('tu_lAma_msg_cut');
      }
      if (!!settings.lamaLoadingBlocker)
        $('body').addClass('tu_lAma_lb');
      if (!!settings.highContrast)
        $('body').addClass('tu_lAma_marc_hc');
    
      if ($('title').text() == 'Md Editor' && document.URL.includes('/rep/MDEditor.jsp')){
        debug('old mdeditor',2).then(function(m){console.log(m)});
        mde = true;
        var state = 0;
        var observer = new MutationObserver(function(mutations) {
          if (!!document.getElementById('tu_lAma_blocker')) return;
          mutations.forEach(function(mutation) {
            if (mutation.removedNodes.length >= 1){
              if (state == 0 && mutation.removedNodes[0].id == "loadingWrapper"){
                state = 1;
                debug('loadingWrapper removed',2).then(function(m){console.log(m)});
              } else if (state == 1 && $(mutation.removedNodes).find('#MdEditor\\.PopupMsg\\.LOADING').length >= 1){
                debug('#MdEditor.PopupMsg.LOADING removed',2).then(function(m){console.log(m)});
                if ($('table.MenuIconBarUxp').length == 0){
                  setTimeout(addButtons,1000);
                } else {
                  addButtons();
                }
                state = 255;
                clearTimeout(obstimer);
                observer.disconnect();
                addMDEColors();
                gotoFirstEmpty();
                catlevel();
                tuwsys();
                checkbug00576546();
                $('body').on("click",function(){
                  browser.runtime.sendMessage({action: 'closeRecentSearches'});
                });
              }
              debug({'rm': mutation.removedNodes[0]},4).then(function(m){console.log(m)});
            }
            if (mutation.addedNodes.length >= 1){
              debug({'ad': mutation.addedNodes[0]}, 4).then(function(m){console.log(m)});
            }
          });
        });
        observer.observe($('body').get(0),{childList:true,subtree:false});
        var obstimer = setTimeout(function(){
          debug('main observer timed out',2).then(function(m){console.log(m)});
          observer.disconnect();
        },60000);
      } else if (document.URL.substr(-18) == 'exlibrisgroup.com/'){
        debug('empty iframe?? ' + document.title,2).then(function(m){console.log(m)});
        debug(document,5).then(function(m){console.log(m)});
        if (document.title == 'Md Editor'){
          newMDEWrapper = true;
          debug('new mde wrapper in empty iframe',2).then(function(m){console.log(m)});
          // das wirkt nicht
          // window.addEventListener("keydown",    keyscan, true);
          if (!!settings.tuwsysUrl || !!settings.gndUrl || isTuw)
            $('body').addClass('tu_lAma_tuwsysac');

          $(document).on(
            'keydown.loadMdeTemplate',
            {t: { value: settings.templateName, action: function (){browser.runtime.sendMessage({action: "triggerLoadMdeTemplate"});}}},
            handleKeyEvent
          );
          $(document).on(
            'keydown.diffViewer',
            {y: { value: 'diffViewer', action: function (){browser.runtime.sendMessage({action: "diffViewer"});}},
             Escape: { value: 'closeDiffViewer', action: function (){browser.runtime.sendMessage({action: "closeDiffViewer"});}}},
            handleKeyEvent
          );
          $(document).on(
            'keydown.save',
            {s: { value: 'save', action: check852c },
             r: { value: 'saverelease', action: check852c },
             i: { value: 'newitem', action: check852c }},
            handleKeyEvent
          );

          $(document).on('click','#MenuItem_menu_records_save, #MenuItem_menu_records_addInventory, #save_dropdown',check852c);

          var gwtHeadObs = new MutationObserver(function(ms){
            ms.forEach(function(m) {
              if (m.type=='characterData'){
                let parent = $(m.target).parent();
                debug(parent,5).then(function(m){console.log(m)});
                /* if (parent.prop('id').startsWith('mdeditor.labels.badges.')){
                  let dsType = parent.prop('id').substr(23,parent.prop('id').length-24);
                  $("[id^='mdeditor.labels.badges.']").closest("[id*='PresenterDiv']").addClass('tu_lAma_DsType_'+dsType);
                  console.log(dsType);
                } */
                if (parent.parent().parent().parent().parent().attr('panelclass') == 'catalogerLevelPanel'){
                  if (m.target.textContent.startsWith('[20]')){
                    $(m.target.parentNode).closest('mat-form-field.catalogerLevelCombo').removeClass('tu_lAma_catLevel');
                  } else {
                    $(m.target.parentNode).closest('mat-form-field.catalogerLevelCombo').addClass('tu_lAma_catLevel');
                  }
                } else if ($(m.target).parent().hasClass('mmsIdContainer')){
                  link2search(0,$(m.target).parent())
                }
              }
            });
          });

          function link2search(i,node){
            node =$(node);
            let match = node.text().match(/\((\d+)\)/);
            node.attr('title','Items/Exemplare');
            node.off('click.ItemSearch');
            node.on('click.ItemSearch',function(){browser.runtime.sendMessage({action: "search", what: "ITEM", id: match[1]});});
            // holdings haben keinen 001
            let h1r = node.closest('[id$="header.firstRow"]');
            let holbib = h1r.parent().find('[id$="header.secondRow"]').text().match(/((Hol)dings|MARC21 (Bib))/);
            let type = (!!holbib[2])?'hol':'bib';
            browser.runtime.sendMessage({action: 'mms_id', value: match[1], tab: h1r.attr('id').substr(0,4), type: type });
            debug(node,5).then(function(m){console.log(m)});
          }

          var mdewobserver = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
              if (mutation.attributeName == 'style' && mutation.oldValue == 'z-index: -9999; visibility: visible;' 
                      && $(mutation.target).attr(mutation.attributeName) == 'z-index: -9999; visibility: hidden;'){
                debug('habe nmde fertig',2).then(function(m){console.log(m)});
                let headhtml = $('#gwt-headers').html();
                browser.runtime.sendMessage({action: 'nMdeLoadingBlockerRemoved'});
                debug(headhtml,5).then(function(m){console.log(m)});
                setTimeout(function(){$('.mmsIdContainer').each(link2search)},500);
                if (!settings.keepCatLevel){
                  gwtHeadObs.disconnect();
                  gwtHeadObs.observe($('#gwt-headers').get(0),{childList:true, subtree: true, characterData:true, characterDataOldValue:true });
                  var matFormField = $('#gwt-headers mat-form-field.catalogerLevelCombo');
                  matFormField.each((i,cl) => {
                    jqcl = $(cl);
                    if (jqcl.text().startsWith('[20]')){
                      jqcl.removeClass('tu_lAma_catLevel');
                    } else {
                      jqcl.addClass('tu_lAma_catLevel');
                    }
                  });
                }
              }
            });
          });
          debug($('div.gwtVisibility'),5).then(function(m){console.log(m)});
          mdewobserver.observe($('div.gwtVisibility').get(0), { attributes: true, attributeOldValue: true, childList:false, subtree:false });
          if (!settings.keepCatLevel && $('#gwt-headers').length > 0)
            gwtHeadObs.observe($('#gwt-headers').get(0),{childList:true, subtree: true, characterData:true, characterDataOldValue:true });

          $('body').on("click",function(){
            browser.runtime.sendMessage({action: 'closeRecentSearches'});
          });
          browser.runtime.onMessage.addListener((request) => {
            debug(request,5).then(function(m){console.log(m)});
            if (request.action == 'lAmaSettingsChanged'){
              browser.storage.local.get().then(function(result){
                settings=result;
              });
            } else if (request.action == 'loadMdeTemplate'){
              debug($('div.cdk-overlay-container'),5).then(function(m){console.log(m)});
              var ltobserver = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                  mutation.addedNodes.forEach(function(node) {
                    if (!!settings.templateName && $(node).find('input#search-text').length > 0){
                      ltobserver.disconnect();
                      $(node).find('input#search-text').attr("tabindex","1").val(settings.templateName).click();
                      $(node).find('button:contains(Ok)').attr("tabindex","2");
                    }
                  });
                });
              });
              if ($('div.cdk-overlay-container').length > 0){
                ltobserver.observe($('div.cdk-overlay-container').get(0), { childList:true, subtree:true });
              } else
                ltobserver.observe($('body').get(0), { childList:true, subtree:true });
            } else if (request.action == 'lang'){
              $('body').attr('lang',request.value);
            }
          });
          browser.runtime.sendMessage({action: 'getLang'});
        }

      } else if (document.URL.includes('/rep/yards_ng.jsp')){
        // gibt s das überhaupt???
        debug('new mde wrapper',2).then(function(m){console.log(m)});
        debug(document,5).then(function(m){console.log(m)});
        $('body').on("click",function(){
          browser.runtime.sendMessage({action: 'closeRecentSearches'});
        });
      } else if (document.URL.includes('/rep/yards.jsp')){
        var nmdeChanged = false;
        var nMdeChangedTimer;
        var mdeChangeObs = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutation) {
            if (nmdeChanged) clearTimeout(nMdeChangedTimer);
              nmdeChanged = true;
              nMdeChangedTimer = setTimeout(function(){
                clearTimeout(nMdeChangedTimer);
                debug('changed nmde '+document.URL,2).then(function(m){console.log(m)});
                // gotoFirstEmpty();
                tuwsys();
                nmdeChanged=false;
              },500);
          });
        });
        function observeChange(){
          mdeChangeObs.disconnect();
          nmdeChanged = false;
          mdeChangeObs.observe($('table[id^="editorTable"] > tbody').get(0),{childList:true,subtree:false});
        }

        if (document.body.childElementCount > 0){
          // new MDE
          mde = newMde = true;
          debug('new mde',2).then(function(m){console.log(m)});
          debug(document,5).then(function(m){console.log(m)});
          if (!!settings.colorScheme){
            debug('add class ' + settings.colorScheme + ' to body',3).then(function(m){console.log(m)});
            $('body').addClass('tu_lAma_ng_marc_' + settings.colorScheme);
          }
          if (!settings.hideSH){
            $('body').addClass('tu_lAma_marc_hidesh');
            $('body').off('click.extlink');
            $('body').on('click.extlink','[id^="FieldTagBox.tag.995"]', function(e){
              let sfu = $(e.target).closest('table').closest('tr').find('textarea').val().trim();
              match = sfu.match(/\$\$u\s*(https?:\/\/[^\s]+)/);
              if (match) window.open(match[1],'_blank');
              return false
            });
          }
          $('body').on("click",function(){
            browser.runtime.sendMessage({action: 'closeRecentSearches'});
          });
          // enable MDE short-cuts
          window.addEventListener("keydown",  keyscan, true);
          browser.runtime.onMessage.addListener((request) => {
            debug(request,5).then(function(m){console.log(m)});
            if (request.action == 'lAmaSettingsChanged'){
              browser.storage.local.get().then(function(result){
                settings=result;
              });
            } else if (request.action == 'nMdeLoadingBlockerRemoved'){ // } || request.action == 'yardsMarcEditorServiceCompleted'){
              // kommen wir hier innerhalb 500 msec nochmal her starten wir den timer neu
              if (nmdeInit) clearInterval(nMdeInitTimer);
              nmdeInit = true;
              var delay = 0;
              nMdeInitTimer = setInterval(function(){
                // während ein bookmarklet läuft machen wir nichts
                // erst wenn sich 15s nichts mehr rührt
                // dann warnen, dass sich das bookmarklet aufgehängt hat und aufräumen???
                if (delay > 30 || $('#tu_lAma_blocker').length == 0){
                  if (delay > 0){
                    debug('bookmarklet-timeout nach '+(delay/2)+'sec',5).then(function(m){console.log(m)});
                    browser.runtime.sendMessage({action: 'bookmarkletTimeout'});
                    $('#tu_lAma_blocker').remove();
                  }
                  clearInterval(nMdeInitTimer);
                  debug('init nmde '+document.URL,2).then(function(m){console.log(m)});
                  gotoFirstEmpty();
                  tuwsys();
                  checkMDEWarnings(settings.warningList);
                  checkbug00576546();
                  nmdeInit=false;
                  if (isTuw && !!settings.tuwsysUrl && settings.tuwsysUrl != '' || !!settings.gndUrl) observeChange();
                }
                delay++;
              },500);
            } else if (request.action == 'triggerLoadMdeTemplate'){
              load_template(settings.templateName);
            } else if (request.action == 'lang'){
              $('body').attr('lang',request.value);
            } else if (request.action == 'diffViewer'){
              createDiff();
            } else if (request.action == 'closeDiffViewer'){
              $('#tu_lAma_diff_viewer').remove();
            } else if (request.action == 'mms_id'){
              $("[id^='editorTable-"+request.tab+"']").attr('lama_mms_id',request.value);
              $("[id^='editorTable-"+request.tab+"']").attr('lama_type',request.type);
            }
          });
          browser.runtime.sendMessage({action: 'getLang'});
        }
      } else {
        newLayout = (document.URL.search('.exlibrisgroup.com/ng') > 0);
        debug(newLayout?'new layout':'old layout',2).then(function(m){console.log(m)});
        var idobserver = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutation) {
            if (mutation.type == "attributes" && mutation.attributeName == "id") {
              enable_key_shortcuts();
              laufzettel();
              if ($('body').is('[id*="record_simple_view"]')){
                addRecordViewLinks();
                addColorTemplate();
              } else {
                $('body').removeClass('tu_lAma_Dublette');
              }

              debug(mutation.target.id,4).then(function(m){console.log(m)});
            }
          });
        });

        idobserver.observe($('body').get(0), { attributes: true, childList:false, subtree:false });

        debug('outside ' + $('body').attr('id') + ' url: ' + document.URL, 2).then(function(m){console.log(m)});
        browser.runtime.onMessage.addListener((request) => {
          debug("Message from the background script:",2).then(function(m){console.log(m)});
          debug(request,2).then(function(m){console.log(m)});
          if (request.action == 'lAmaSettingsChanged'){
            browser.storage.local.get().then(function(result){
              settings=result;
            });
          } else if (request.action == 'search'){
            setSimpleSearchKey(request.what);
            $('#ALMA_MENU_TOP_NAV_Search_Text').val(request.id).trigger('change');
            $('button#simpleSearchBtn').trigger('click');
          } else if (request.action == 'historyStateUpdated'){

            debug($('body').attr('id'),3).then(function(m){console.log(m)});

            enable_key_shortcuts();
            // laufzettel();
          } else if (request.action == 'toast'){
            toast(request.message, request.color, request.duration);
          } else if (request.action == 'closeRecentSearches'){
            $('#recentSearchesContainer').removeClass('open');
          } else if (request.action == 'checkLocation'){
            if (!!settings.locationIPURL && settings.selectedLocation != undefined && settings.selectedLocation[1] != undefined && $('location-pinable-menu button').first().text() != settings.selectedLocation[1]){
              toast('<h3 style="color: black;">Bitte den Standort auf '+ settings.selectedLocation[1] + ' ändern!</h3>', wcols[5],20);
              $('location-pinable-menu button').get(0).dispatchEvent(click);
            }
          } else if (request.action == 'getLang'){
            browser.runtime.sendMessage({action: 'lang', value: $('html').attr('lang')});
          }
        });
        enable_key_shortcuts();

        $('#MENU_LOCATION_LIST_hiddenSelect, #MENU_LOCATION_LIST').on('change',function(){
          debug(this.value,2).then(function(m){console.log(m)});
        });
        $('html').on('hashchange',function(e){
          debug(this,3).then(function(m){console.log(m)});
          debug(e,3).then(function(m){console.log(m)});
        });

        var lbsiobserver = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutation) {
            if (mutation.target.className == 'hide' || mutation.target.className == 'hidden'){
              laufzettel();
              if ($('body').is('[id*="record_simple_view"]')){
                addRecordViewLinks();
                addColorTemplate();
              } else {
                $('body').removeClass('tu_lAma_Dublette');
              }
            }
          });
        });
        if ($('#loadingBlockerStatusIdentifier').length > 0)
          lbsiobserver.observe($('#loadingBlockerStatusIdentifier').get(0),{childList:false,subtree:false,attributes:true});
        else
          debug('no loading blocker!',4).then(function(m){console.log(m)});

        toast();
      }
    });
    debug('TU lAma loaded ' + document.URL,0).then(function(m){console.log(m)});
  } else {
    debug('primo iframe ignored',1).then(function(m){console.log(m)});
  }
});
