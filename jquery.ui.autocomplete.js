/* 
  TU lAma - let Alma be more adroit

  extends jquery.ui.autocomplete.html
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

(function( $ ) {

  var proto = $.ui.autocomplete.prototype;

  $.extend( proto, {

    _renderMenu: function(ul, items){
      var self = this;

      if (items.length > 0 && items[0].counts != undefined){
        if(!!items[0].link){
          $("<li class='autocomplete_title'><b><a target='KonkordanzSuche' href='"+items[0].link + "'>"+items[0].counts.limit
           +" Treffer von ("+items[0].counts.count+(!!items[0].counts.titles?"/"+items[0].counts.titles:'')+")</a></b></li>").appendTo(ul);
          try {
            var query = new URL(items[0].link).searchParams.get('query');
            if (self.element[0].nodeName == 'TEXTAREA' && self.element[0].value == '$$a ')
              self.element[0].value = '$$a ' + query;
          } catch (e) { console.log(e); }
        } else
          $("<li class='autocomplete_title'><b>"+items[0].counts.limit+" Titel von ("
           +items[0].counts.count+(!!items[0].counts.titles?"/"+items[0].counts.titles:'')+")</b></li>").appendTo(ul);
      }
      $.each(items, function (index, item) {
                         
        if (item.sub != undefined){
          $.each(item.sub, function (index, sub) {
            sub.class = 'tuwsub';
            self._renderItemData(ul, sub);
          });
        }
        self._renderItemData(ul, item);
      });
    }
  });

})( jQuery );
