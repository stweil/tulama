# Allgemeine Funktionen

* Warnung bei falschem Standort je nach lokaler IP Adresse  
JSON Antwort von URL: https://almagw.ub.tuwien.ac.at/tulama/locMap.php  
['Standort_ID', 'Standort Beschreibung']
* Liste von Suchbegriffen erweitern (AC-Nummern, MMS-IDs, ISBN):  
Liste ins Suchfeld kopieren,  
mit **`Strg-Alt-Y`** wird auf die "erweiterte Suche" umgestellt  
hier eventuell von "Stichwörter" auf entsprechendes Feld einschränken  
mit nochmals **`Strg-Alt-Y`** wird die Suche erweitert und alle Suchbegriffe einzeln mit "oder" verknüpft.  
Je nach CPU und RAM Ausstattung auch mit ein paar hundert Werten (i5 8th gen, 8GB RAM 500Werte ~30s)
* Farbliche Markierung von Tags Indikatoren und Subfeldern in Marc21 Datensätzen  (wie in Michael Birkners AlEn) mit verschiedenen Farbprofilen
* Hoher Kontrast - wirklich schwarze Schrift
* Warnung bei als Dubletten bzw. zum Löschen markierten Datensätzen (970)
* Laden von Bookmarklets (Makros) über eine externe URL
* Sortieren in der "Liste der Exemplaren" nach Beschreibung **`Strg-Alt-D`**
* Leeres 852_8_c beim Speichern ergänzen ("Speichern" und "neues Exemplar" Button) (nur im alten MDE)
* Statusmeldungen an den unteren Rand verschieben
* lAma Loading Blocker lAma statt dem Kreisel; ein und ausschalten mit (Shift-)-**`Strg-Alt-L`**  
Achtung: im MDE und und in diversen Ergebnislisten anders belegt!


# MD-Editor

* Automatisch zum ersten leeren Feld springen
* Standard Template für "Aus Vorlage Erweitern" mit **`Strg-Alt-T`**   
neuer MDE: Die konfigurierte Vorlage wird vorausgefüllt.  
Mit einmal **`Backspace`** wird die Liste gefiltert  
und wenn man nicht zur Maus greifen will:  
mit **`↓`** und **`Enter`** gewünschte Vorlage auswählen,  
mit **`Tab`** auf "Ok" und nochmal **`Enter`**
* Warnung bei Katalogisierungs-Ebene != 20

# Exemplar Editor

* Vorlage für Standardwerte anwenden **`Strg-Alt-T`**, Werte ändern mit **`Shift-Strg-Alt-T`**
* Exemplar-Richtlinie löschen bzw. ändern **`Strg-Alt-C`**, Wert ändern mit **`Shift-Strg-Alt-C`**
* Prozesstype ändern **`Strg-Alt-B`**, Wert ändern mit **`Shift-Strg-Alt-B`**

# TU-Interne Erweiterungen

* Entlehnzahlen in der "Liste der Exemplare" und der Bestandsliste einblenden (Shift-)**Strg-Alt-L**
* TU-Systematik Autocomplete:  
beim Schreiben in 983 wird automatisch die TU-Systematik im Klartext ergänzt  
bei der Eingabe von bis zu 3 Zeichen werden nur HG-Kürzel durchsucht  
d.h. um z.B. alle 011 zu finden ":011" eingeben  
Achtung! Immer zumindest den endgültigen Autocomplete Vorschlag mit **`↓`** **`↑`**  und Enter oder mit der Maus auswählen  
**Ergänzen von Zusatzcodes:**  
**`Strg-Alt-Ö`** (Shift-Strg-Alt-M) Ortskürzel: der Ländercode wird wenn bekannt automatisch ergänzt  
**`Strg-Alt-G`**: Ländercode  
**`Strg-Alt-L`**: Sprachcode  
Strg-Alt-N: Namenskürzel  
Shift-Strg-Alt-Q: Verweis auf andere Hauptgruppe  
(Shift-)**`Strg-Alt-P`**: Hauptgruppen-Liste erzeugen  
**`Strg-Alt-D`**: aktuelles Feld leeren  
**`Strg-Alt-I`**: Konkordanzsuche über alle ISBNs und Titel im Datensatz  
**`Strg-Alt-K`**: Konkordanzsuche über ausgewählten Text, oder aktuellen Text im 983  
  Eine Liste von Titeln wird angezeigt,  
  mit Klick auf den Titel wird die komplette Systematik übernommen  
  mit Klick auf eine einzelne Systematik nur diese.  
  Einschränkung auf Titel mit bk bzw Bestimmter Systematik:  
  983 $$a Transputer bk BAU:  
  *Erweiterte Suchsyntax:**  
  werden keine Modifikatoren benutzt werden alle Begriffe automatisch mit "+" und "*" ergänzt  
  z.B.: +Transputer* +BAU:*  
  **`+`** der Begriff muss vorkommen  
  **`*`** Wortanfänge  
  **`-`** der Begriff darf nicht vorkommen  
  *Ethik Trunkierung am Wortanfang nur mit einem Begriff möglich
      ...
* Automatische ISBN-Suche und Titel bei mind. 8 leeren 983
* Vergleich der aktuell geladenene lokalen Felder mit früher bekannten Versionen (Missing lokal Fields)
