obj := $(wildcard *.js) $(wildcard icons/*.*) $(wildcard *.css) manifest.json CHANGELOG README.md LICENSE $(wildcard _locales/*/*.*) $(wildcard templates/*.*) $(wildcard vendor/*.js) $(wildcard settings/*.*)

zip: $(obj)
	zip -r -x \*.less -x README.md -x CHANGELOG -x make.sh -x *.swp -x Makefile -FS ../tulama.zip *	

color.css: color.less coltemplate.less marc21col.less marc21ngcol.less
	lessc color.less color.css
