/* 
  TU lAma - let Alma be more adroit

  settings script
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

function saveOptions(e) {
  $('body').css({opacity: 0.5});
  e.preventDefault();

  var warningValues = $('tbody.warningValues');
  var warningList = new Array;
  warningValues.each(function(i,e){
    if ($(e).find('#warningRegExp').val() != ''){
      try {
        var re=new RegExp($(e).find('#warningRegExp').val(), $(e).find('#warningReFlag').val());
        warningList.push({
          tag: $(e).find('#warningTag').val(),
          ind1: $(e).find('#warningInd1').val(),
          ind2: $(e).find('#warningInd2').val(),
          re: re,
          level: $(e).find('#warningLevel').val(),
          msg: $(e).find('#warningMsg').val(),
          color: $(e).find('#warningColor').val(),
          not: $(e).find('#warningNot').prop("checked"),
          bib: $(e).find('#warningBib').prop("checked"),
          hol: $(e).find('#warningHol').prop("checked")
        });
      } catch(e){
        alert("Fehler in der Regular Expression!\n\n"+e);
        console.log(e);
        return false;
      }
    }
  });

  let locationIPURL = document.querySelector('#locationIPURL').value;

  browser.storage.local.set({
    loansUrl: document.querySelector('#loansUrl').value,
    bmsUrl: document.querySelector('#bmsUrl').value,
    tuwsysUrl: document.querySelector('#tuwsysUrl').value,
    gndUrl: $('#gndUrl').prop("checked")?$('#gndUrl').val():false,
    lobidUrl: $('#lobidUrl').prop("checked")?$('#lobidUrl').val():false,
    creatorAC: $('#creatorAC').prop("checked"),
    processSlip: $('#processSlip').prop("checked"),
    lockPriKey: document.querySelector('#lockPriKey').value,
    unlockPriKey: document.querySelector('#unlockPriKey').value,
    disabledBmlsCats: $('#bmsCats').val(),
    keepCatLevel: !$('#keepCatLevel').prop("checked"),
    checkBug00576546: !$('#checkBug00576546').prop("checked"),
    goto1stEmpty: !$('#goto1stEmpty').prop("checked"),
    addLinks: !$('#addLinks').prop("checked"),
    addPortfolio: !!$('#addPortfolio').prop("checked"),
    templateName: document.querySelector('#templateName').value,
    add852c: document.querySelector('#add852c').value,
    locationIPURL: locationIPURL,
    colorScheme: $('#colorScheme').val(),
    overwriteAlmaColors: $('#overwriteAlmaColors').prop("checked"),
    highContrast: $('#highContrast').prop("checked"),
    messageToBottom: !$('#messageToBottom').prop("checked"),
    messageCut: $('#messageCut').prop("checked"),
    hideSH: !$('#hideSH').prop("checked"),
    debugLevel: $('#debugLevel').val(),
    warningList: warningList
  }).then(function(){
    // check origins permissions
    // and open permissions tab if needed, requesting new permissions is not working in about:addons
    var permissions = [];
    var origins = [];
    for (let urlId of ['bmsUrl','loansUrl','tuwsysUrl','locationIPURL','gndUrl','lobidUrl']){
      var origin = document.querySelector('#'+urlId).value;
      if (origin != '' && (urlId != 'gndUrl' || $('#gndUrl').prop("checked")) && (urlId != 'lobidUrl' || $('#lobidUrl').prop("checked"))){
        if (origin.substr(-1) != '/') origin = origin + '/';
        origins.push(origin);
        // if (urlId == 'locationIPURL') permissions.push("webRequestBlocking");
      }
    }
    if (tuwSeen) origins.push('https://tiss.tuwien.ac.at/');
    if (origins.length > 0){
      try{
        browser.permissions.contains({origins: origins,permissions: permissions}).then((result) => {
          if (!result){
            browser.tabs.create({
              url: browser.runtime.getURL("permissions.html")
            });
          }
        });
      } catch(err){alert(err)}
    }
  });

  if (locationIPURL != undefined && locationIPURL != ''){
    if (locationIPURL == 'https://almagw.ub.tuwien.ac.at/tulama/locMap.json' || locationIPURL == 'https://almagw.ub.tuwien.ac.at/tulama/locMapName.json'){
      locationIPURL = 'https://almagw.ub.tuwien.ac.at/tulama/locMap.php'
      browser.storage.local.set({locationIPURL:locationIPURL});
    }
    var xhrlm = new XMLHttpRequest();
    console.log('loaded IP to Location map from ' + locationIPURL);
    xhrlm.responseType = 'json';
    xhrlm.open("GET", locationIPURL + "?nocache_"+(new Date()).getTime());
    xhrlm.onreadystatechange = function () {
      if (xhrlm.readyState === 4 && xhrlm.status === 200) {
        console.log(xhrlm.response);
        browser.storage.local.set({selectedLocation: xhrlm.response});
      }
    };
    xhrlm.send(null);
  }
  browser.tabs.query({url: "*://*.exlibrisgroup.com/*"}).then(
    function (tabs) {
      for (let tab of tabs) {
        browser.tabs.sendMessage(
          tab.id,
          { action: 'lAmaSettingsChanged' }
        );
      }
    }
  );
  $('body').delay(500).animate({opacity: 1});
}

var tuwSeen;

function restoreOptions() {
  
  var warningList = $('tbody.warningValues').first().clone();
  console.log(warningList);
  $('#addWarningRule').on('click',addWarningRule);
  function clearWarningRule(){
    console.log(this);
    $(this).closest('tbody').remove();
  }
  function addWarningRule(){
    console.log(warningList);
    $('table#warningList').append(warningList.clone());
    $('table#warningList #clearWarningRule').on('click',clearWarningRule);
  }

  function setCurrentChoice(result) {
    tuwSeen = !!result.tuwSeen;
    if (!!result.tuwSeen) $('body').removeClass('tuw');
    if (!result.bmsUrl && !!result.tuwSeen || result.bmsUrl == 'https://almagw.ub.tuwien.ac.at/ubintern/bmls/')
      result.bmsUrl = 'https://almagw.ub.tuwien.ac.at/tulama/bmls/';
    document.querySelector('#loansUrl').value = result.loansUrl || '';
    document.querySelector('#bmsUrl').value = result.bmsUrl || '';
    document.querySelector('#tuwsysUrl').value = result.tuwsysUrl || '';
    $('#processSlip').prop("checked", !!result.processSlip).trigger('change');
    document.querySelector('#lockPriKey').value = result.lockPriKey || '';
    document.querySelector('#unlockPriKey').value = result.unlockPriKey || '';
    // document.querySelector('#').value = result. || '';
    document.querySelector('#templateName').value = result.templateName || '0_SE-3-SYS';
    document.querySelector('#add852c').value = result.add852c || 'UNASSIGNED';
    $('#selectLocation').prop("checked",!result.selectLocation).trigger('change');
    $('#hideSH').prop("checked",!result.hideSH).trigger('change');
    console.log(result);
    
    if (!!result.warningList){
      var wlTable = $('table#warningList');
      for (var i=0; i<result.warningList.length; i++){
        wlTable.append(warningList.clone());
      }
      $('table#warningList #clearWarningRule').on('click',clearWarningRule);
      var warningValues = $('tbody.warningValues');
      for (var i=0; i<result.warningList.length; i++){      
        var line = $(warningValues.get(i));
        var vals = result.warningList[i]; 
        line.find('#warningTag').val(vals.tag);
        line.find('#warningInd1').val(vals.ind1);
        line.find('#warningInd2').val(vals.ind2);
        line.find('#warningRegExp').val(vals.re.source);
        line.find('#warningReFlag').val(vals.re.flags);
        line.find('#warningColor').val(vals.color);
        line.find('#warningLevel').val(vals.level);
        line.find('#warningMsg').val(vals.msg);
        line.find('#warningNot').prop("checked",!!vals.not);
        line.find('#warningBib').prop("checked",!!vals.bib);
        line.find('#warningHol').prop("checked",!!vals.hol);
      }
    }
    var cats = "<select multiple='multiple' id='bmsCats' name='bmsCats' size='1'>\n";
    if (!!result.bmlsCats){
      result.bmlsCats.forEach(function(cat){ cats += "<option id='" + cat.cat + "' title='"+cat.tit+"' value='" + cat.cat + "'>"+ cat.cat + "</option>\n"; });
    }
    var msel = $(cats + "</select>");
    $('#bmsCatsTd').empty().append(msel);
    if (!result.disabledBmlsCats){ result.disabledBmlsCats = [ 'onetime','hss','stb','erw' ]; }
    if (!!result.bmlsCats){
      result.disabledBmlsCats.forEach(function(cat){$('option#'+cat).prop('selected',true).trigger('change');});
    }
    if (!!result.colorScheme){
      $('#colorScheme option#'+result.colorScheme).prop('selected',true).trigger('change');
    }
    if (!!result.debugLevel){
      $('#debugLevel option#'+result.debugLevel).prop('selected',true).trigger('change');
    }
    $('#overwriteAlmaColors').prop("checked",!!result.overwriteAlmaColors).trigger('change');
    $('#highContrast').prop("checked",!!result.highContrast).trigger('change');
    $('#keepCatLevel').prop("checked",!result.keepCatLevel).trigger('change');
    $('#checkBug00576546').prop("checked",!result.checkBug00576546).trigger('change');
    $('#goto1stEmpty').prop("checked",!result.goto1stEmpty).trigger('change');
    $('#addLinks').prop("checked",!result.addLinks).trigger('change');
    $('#addPortfolio').prop("checked",!!result.addPortfolio).trigger('change');
    $('#messageToBottom').prop("checked",!result.messageToBottom).trigger('change');
    $('#messageCut').prop("checked",!!result.messageCut).trigger('change');
    $('#gndUrl').prop("checked",!!result.gndUrl).trigger('change');
    $('#lobidUrl').prop("checked",!!result.lobidUrl).trigger('change');
    if (!result.lobidUrl && !result.gndUrl) $('#creatorAC').attr('disabled','disabled');
    else $('#creatorAC').prop("checked",!!result.creatorAC).trigger('change');
    document.querySelector('#locationIPURL').value = result.locationIPURL || '';

    $('#gndUrl,#lobidUrl').on('change',function(){
      $('#creatorAC').prop('disabled', !$('#gndUrl').prop("checked") && !$('#lobidUrl').prop("checked"));
    });
  }
  function onError(error) {
    console.log('Error: ${error}')
  }
  var getting = browser.storage.local.get();
  getting.then(setCurrentChoice, onError)
}
document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector('form').addEventListener('submit', saveOptions);
$('[data-i18n]').each(function () {
  var dataI18n = $(this).data('i18n');
  // var innerHtml = $(this).html();
  $(this).html(browser.i18n.getMessage(dataI18n))
});
